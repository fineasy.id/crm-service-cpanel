-- application gate view
DROP VIEW IF EXISTS `_application_gate`;
CREATE VIEW `_application_gate` AS
SELECT
    a.contact_id,
	  t.*,
	  getValidLoanSeq(t.product_id,t.loan_seq) valid_seq,
    p.name as product_name,
    m.name as marital_status_name,
    c.name as children,
    u.name as loan_purpose,
    b.name as business_years,
    s.name as status_name,
    e.name as education,
    s.on_process
FROM
	`application_tmp` t
	  JOIN application_status s ON (t.status_id=s.id)
    LEFT JOIN `product` p ON (t.product_id=p.product_id)
    LEFT JOIN contact_marital_status m ON (t.marital_status_id=m.id)
    LEFT JOIN contact_children c ON (t.children_id=c.id)
    LEFT JOIN application_loan_purpose u ON (t.purpose_id=u.id)
    LEFT JOIN contact_business_years b ON (t.business_years_id=b.id)
    LEFT JOIN contact_edu e ON (t.education_id=e.id)
    LEFT JOIN contact a ON (a.easypay_id=t.easypay_id)
WHERE 1;


-- aplication view
DROP VIEW IF EXISTS `_application`;
CREATE VIEW `_application` AS
SELECT
    a.full_name,
    a.mobile,
    a.reg_id,
    a.email,
    a.birth_date,
    a.mother_name,
    a.home_addr,
    a.business_addr,
    a.business_years_id,
    a.easypay_id,
    a.monthly_turnover,
    a.education_id,
    a.children_id,
    a.marital_status_id,
    a.ref_id,
	  t.*,
	  getValidLoanSeq(t.product_id,t.loan_seq) valid_seq,
    p.name as product_name,
    m.name as marital_status_name,
    c.name as children,
    u.name as loan_purpose,
    b.name as business_years,
    s.name as status_name,
    e.name as education,
    n.name as channel_name,
    s.on_process,
    c1.full_name as ins_by_name,
    c2.full_name as mod_by_name
FROM
	  `application` t
	  JOIN contact c1 ON (c1.contact_id=t.ins_by)
    JOIN contact c2 ON (c2.contact_id=t.mod_by)
	  JOIN contact a ON (t.contact_id=a.contact_id)
	  JOIN application_status s ON (t.status_id=s.id)
	  JOIN application_channel n ON (t.channel_id=n.id)
    LEFT JOIN `product` p ON (t.product_id=p.product_id)
    LEFT JOIN contact_marital_status m ON (a.marital_status_id=m.id)
    LEFT JOIN contact_children c ON (a.children_id=c.id)
    LEFT JOIN application_loan_purpose u ON (t.purpose_id=u.id)
    LEFT JOIN contact_edu e ON (a.education_id=e.id)
    LEFT JOIN contact_business_years b ON (a.business_years_id=b.id)
WHERE 1;


-- application log
DROP VIEW IF EXISTS `_application_verification_log`;
CREATE VIEW `_application_verification_log` AS
SELECT
	l.*,
	s.name as status_name,
  c.full_name as created_by
FROM
	`application_verification_log` l
    JOIN application_status s ON (s.id=l.status_id)
    JOIN contact c ON (c.contact_id=l.ins_by)
WHERE 1
ORDER BY l.ins_on;

-- contract
-- aplication view
DROP VIEW IF EXISTS `_contract`;
CREATE VIEW `_contract` AS
SELECT
    o.*,
    t.product_id,
	  t.contact_id,
    t.loan_seq,
	  t.amount_req,
	  t.term_req,
    a.full_name,
    a.mobile,
    a.reg_id,
    a.email,
    a.birth_date,
    a.mother_name,
    a.home_addr,
    a.business_addr,
    a.business_years_id,
    a.easypay_id,
    a.monthly_turnover,
    a.education_id,
    a.children_id,
    a.marital_status_id,
    a.ref_id,
	  getValidLoanSeq(t.product_id,t.loan_seq) valid_seq,
    p.name as product_name,
    m.name as marital_status_name,
    c.name as children,
    u.name as loan_purpose,
    b.name as business_years,
    s.name as status_name,
    e.name as education,
    s.is_active,
    c1.full_name as ins_by_name,
    c2.full_name as mod_by_name,
    (o.daily_charge*o.term_approved) as total_debt
FROM
    contract o
    JOIN contact c1 ON (c1.contact_id=o.ins_by)
    JOIN contact c2 ON (c2.contact_id=o.mod_by)
	  JOIN `application` t ON (o.app_id=t.app_id)
	  JOIN contact a ON (t.contact_id=a.contact_id)
	  JOIN contract_status s ON (o.status_id=s.id)
    LEFT JOIN `product` p ON (t.product_id=p.product_id)
    LEFT JOIN contact_marital_status m ON (a.marital_status_id=m.id)
    LEFT JOIN contact_children c ON (a.children_id=c.id)
    LEFT JOIN application_loan_purpose u ON (t.purpose_id=u.id)
    LEFT JOIN contact_edu e ON (a.education_id=e.id)
    LEFT JOIN contact_business_years b ON (a.business_years_id=b.id)

WHERE 1;


-- cashflow
DROP VIEW IF EXISTS `_cashflow`;
CREATE VIEW `_cashflow` AS
SELECT
  c.*,
  t.name as type_name,
  e.name as category_name,
  s.name as status_name,
  c1.full_name as ins_by_name,
  c2.full_name as mod_by_name
FROM
  cashflow c
  JOIN cashflow_type t ON (c.type_id=t.id)
  JOIN cashflow_category e ON (e.id=c.category_id)
  JOIN cashflow_status s ON (s.id=c.status_id)
  JOIN contact c1 ON (c1.contact_id=c.ins_by)
  JOIN contact c2 ON (c2.contact_id=c.mod_by)
WHERE 1


DROP VIEW IF EXISTS `_telesales_application_incomplete`;
CREATE VIEW `_telesales_application_incomplete` AS
SELECT
    a.full_name,
    a.mobile,
    a.reg_id,
    a.email,
    a.birth_date,
    a.mother_name,
    a.home_addr,
    a.business_addr,
    a.business_years_id,
    a.easypay_id,
    a.monthly_turnover,
    a.education_id,
    a.children_id,
    a.marital_status_id,
    a.ref_id,
	  t.product_id,
	  t.loan_seq,
	  getValidLoanSeq(t.product_id,t.loan_seq) valid_seq,
    p.name as product_name,
    m.name as marital_status_name,
    c.name as children,
    u.name as loan_purpose,
    b.name as business_years,
    s.name as status_name,
    e.name as education,
    s.on_process,
    q.processed,
    q.id as queue_id
FROM
    telesales_queue q
    JOIN application_tmp t ON (t.tmp_id=q.application_tmp_id)
	  JOIN contact a ON (q.contact_id=a.contact_id)
	  JOIN application_status s ON (t.status_id=s.id)
    LEFT JOIN `product` p ON (t.product_id=p.product_id)
    LEFT JOIN contact_marital_status m ON (a.marital_status_id=m.id)
    LEFT JOIN contact_children c ON (a.children_id=c.id)
    LEFT JOIN application_loan_purpose u ON (t.purpose_id=u.id)
    LEFT JOIN contact_edu e ON (a.education_id=e.id)
    LEFT JOIN contact_business_years b ON (a.business_years_id=b.id)
WHERE 1;


-- contract summary
DROP VIEW IF EXISTS `_contract_summary`;
CREATE VIEW `_contract_summary` AS
SELECT
  s.*,
  i.name,
  i.display_order,
  i.bold
FROM
  contract_summary s
  JOIN contract_summary_items i ON (s.item_id=i.id)
ORDER BY
  s.contract_id,
  i.display_order,
  i.id


-- contract deducation
DROP VIEW IF EXISTS `_contract_deduction`;
CREATE VIEW `_contract_deduction` AS
SELECT
  d.*,
  amount+prev_balance-repayment as remain_amount
FROM
  contract_deduction d
WHERE 1
ORDER BY
  d.contract_id,
  d.seq


-- collection call result
DROP VIEW IF EXISTS `_collection_call_log`;
CREATE VIEW `_collection_call_log` AS
SELECT
	l.*,
    r.name as result_name,
    c1.full_name as ins_by_name
FROM
	`collection_call_log` l
    JOIN collection_call_result r ON (l.result_id=r.id)
    JOIN contact c1 ON (c1.contact_id=l.ins_by)
WHERE 1;

-- verification queue
DROP VIEW IF EXISTS _application_verification_queue;
CREATE VIEW _application_verification_queue AS
SELECT a.*,MAX(l.ins_on) as last_verification FROM _application a LEFT JOIN  application_verification_log l ON (a.app_id=l.app_id)
 WHERE on_process=1 AND (a.callback_on IS NULL OR a.callback_on<=NOW())
 AND a.app_id NOT IN (SELECT app_id FROM application_verification_reserve)
 GROUP BY a.app_id
 ORDER BY last_verification,a.mod_on;

-- contact stats
DROP VIEW IF EXISTS _contact;
CREATE VIEW _contact AS
SELECT
	o.*,
    (SELECT COUNT(*) FROM _application WHERE contact_id=o.contact_id AND on_process=1) as app_active,
    (SELECT COUNT(*) FROM _contract WHERE contact_id=o.contact_id) as contract_num,
    (SELECT COUNT(*) FROM _contract WHERE contact_id=o.contact_id AND is_active=1) as contract_active,
    (SELECT COUNT(*) FROM _contract WHERE contact_id=o.contact_id AND is_active=0 AND status_id IN (401,402)) as contract_closed,
    (SELECT MAX(closed_on) FROM _contract WHERE contact_id=o.contact_id AND is_active=0 AND status_id IN (401,402)) as last_closed
FROM
     contact o


-- contact eligible
DROP VIEW IF EXISTS _contact_eligible_repeat;
CREATE VIEW _contact_eligible_repeat AS
SELECT * FROM _contact
WHERE
  app_active=0
  AND contract_active = 0
  AND contract_closed > 0
  AND dpd_max <= CAST((SELECT cfg_val FROM config WHERE cfg_key='repeat.max.dpd') AS INT);

-- contact eligible
DROP VIEW IF EXISTS _contact_non_eligible_repeat;
CREATE VIEW _contact_non_eligible_repeat AS
SELECT * FROM _contact
WHERE
  app_active=0
  AND contract_active = 0
  AND contract_closed > 0
  AND dpd_max > CAST((SELECT cfg_val FROM config WHERE cfg_key='repeat.max.dpd') AS INT);