<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.form.FieldSet",
    onVerification: false,
    title: "Loan Detail",
    defaultType: 'textfield',
    viewOnly: false,
    padding: '0 0 10 10',
    defaults: {
        allowBlank: false
    },
    appData: {},
    initComponent: function() {
        var me = this;
        var appData = me.appData;

        var productTerm = {
            "productId": (appData)?appData.product_id:"",
            "seq": (appData)?appData.loan_seq:""
        }

        var applicationListeners = {
            change: function(f) {
                f.changed = true;
            },
            blur: function(f) {
                if (!f.changed) return;
                if (!f.isValid()) return;
                if (f.getValue()=="") return;
                var fieldName = f.getName();
                var fieldValue = f.getValue();
                var url = '<c:url value="/data/application/PhoneVerification/updateApplication.json" />';
                f.changed = false;
                updateField(f, url, fieldName, fieldValue)
            }
        }

        var updateField = function(field, url, fieldName, fieldValue) {
            Ext.Ajax.request({
                url: url,
                method: 'POST',
                params: {
                    appId: appData.app_id,
                    contactId: appData.contact_id,
                    fieldName: fieldName,
                    fieldValue: fieldValue,
                    "${_csrf.parameterName}": "${_csrf.token}"
                },
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                    if (!obj.success) {
                        Ext.Msg.alert('Alert', obj.msg, function(){
                            field.reset();
                        });
                    }
                },
                failure: function(transport){
                    alert("Error: " - transport.responseText);
                }
            });
        }

        var reloadDeduction = function() {
            if (comboApprovedAmount.getValue() == "" || comboApprovedAmount.getValue() <= 0) return;
            if (comboApprovedTerm.getValue() == "" || comboApprovedTerm.getValue() <= 0) return;

            Ext.Ajax.request({
                url: '<c:url value="/data/application/PhoneVerification/getDeductionAmount.json" />',
                method: 'POST',
                params: {
                    "productId": (appData)?appData.product_id:1001,
                    "loanSeq": (appData)?appData.valid_seq:1,
                    "amount": comboApprovedAmount.getValue(),
                    "tenor": comboApprovedTerm.getValue(),
                    "${_csrf.parameterName}": "${_csrf.token}"
                },
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                    if (obj.success) {
                        fieldDeductionAmount.setValue(obj.charge);
                        endOn.setValue(obj.end_on);
                    }
                },
                failure: function(transport){
                    alert("Error: " - transport.responseText);
                }
            });
        }

        var comboReqAmount = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Amount',
            readOnly: ((appData.amount_req||me.viewOnly) && !me.onVerification)?true:false,
            autoLoad: true,
            value: (appData.amount_req)?appData.amount_req:"",
            name: "amount_req",
            extraParams: productTerm,
            url: "<c:url value="/data/misc/DataComboBox/amountList.json" />",
            listeners: applicationListeners
        });

        var comboReqTerm = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Term',
            autoLoad: true,
            name: "term_req",
            value: (appData.term_req)?appData.term_req:"",
            readOnly: ((appData.amount_req||me.viewOnly) && !me.onVerification)?true:false,
            extraParams: productTerm,
            url: "<c:url value="/data/misc/DataComboBox/termList.json" />",
            listeners: applicationListeners
        });

        var comboApprovedAmount = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Amount',
            disabled: (appData.amount_approved)?false:true,
            autoLoad: true,
            readOnly: (me.viewOnly && !me.onVerification)?true:false,
            value: (appData.amount_approved)?appData.amount_approved:"",
            name: "!amount_approved",
            extraParams: productTerm,
            url: "<c:url value="/data/misc/DataComboBox/amountList.json" />",
            listeners: {
                change: function(c, newValue, oldValue, opts) {
                    if (newValue=="") return;
                    if (fieldTurnover.getValue()=="") return;
                    Ext.Ajax.request({
                        url: '<c:url value="/data/application/PhoneVerification/checkAmountIsValid.json" />',
                        method: 'POST',
                        params: {
                            "productId": (appData)?appData.product_id:1001,
                            "loanSeq": (appData)?appData.valid_seq:1,
                            "turnover": fieldTurnover.getValue(),
                            "amount": newValue,
                            "${_csrf.parameterName}": "${_csrf.token}"
                        },
                        success: function(response){
                            var obj = Ext.decode(response.responseText);
                            if (obj.success == 0) {
                                Ext.Msg.alert('Amount not valid', 'Allowed amount is Rp '+obj.amount, function(){
                                    if (obj.amount <= 0) c.setValue("");
                                    else c.setValue(obj.amount);
                                });
                            }
                            comboApprovedTerm.setDisabled(false);
                            reloadDeduction();
                        },
                        failure: function(transport){
                            alert("Error: " - transport.responseText);
                        }
                    });
                }
            }
        });

        var comboApprovedTerm = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Term',
            autoLoad: true,
            name: "!term_approved",
            readOnly: (me.viewOnly&& !me.onVerification)?true:false,
            value: (appData.term_approved)?appData.term_approved:"",
            disabled: true,
            extraParams: productTerm,
            url: "<c:url value="/data/misc/DataComboBox/termList.json" />",
            listeners: {
                select: function (c, r, opts) {
                    reloadDeduction();

                }
            }
        });

        var fieldTurnover = Ext.create("Ext.form.field.Text",{
            fieldLabel: 'Monthly turnover#',
            name: "#monthly_turnover",
            minLength:7,
            readOnly: (me.viewOnly&& !me.onVerification)?true:false,
            maxLength: 8,
            value: (appData)?appData.monthly_turnover:"", maskRe: /[0-9]/,
            listeners: {
                change: function(f, newValue, oldValue, opts) {
                    comboApprovedTerm.setValue("");
                    comboApprovedAmount.setValue("");
                    fieldDeductionAmount.setValue("");
                    endOn.setValue("");
                    if (newValue.length>=6 && newValue.length<=8) {
                        comboApprovedAmount.setDisabled(false);
                    } else {
                        comboApprovedAmount.setDisabled(true);
                    }
                }
            }
        });



        var fieldDeductionAmount = Ext.create("Ext.form.field.Text",{
            fieldLabel: 'Daily deduction',
            name: "!daily_charge",
            value: (appData.daily_charge)?appData.daily_charge:"",
            readOnly: true
        });

        var endOn = Ext.create("Ext.form.field.Text",{
            fieldLabel: 'End date deduction',
            name: "!contract_end_on",
            value: (appData.contract_end_on)?appData.contract_end_on:"",
            readOnly: true
        });

        Ext.apply(this,{
            items: [
                Ext.create("component.abstract.GenericFieldContainer",{

                    items:[
                        {
                            fieldLabel: 'Application ID#',  name: "app_id", value: (appData)?appData.app_id:"", readOnly: true
                        },{
                            fieldLabel: 'Created On', name: "_ins_on", readOnly: true, value: (appData)?appData.ins_on:""
                        },
                        {
                            fieldLabel: 'Modified On', name: "_mod_on", readOnly: true, value: (appData)?appData.mod_on:"", colspan:2
                        },{
                            fieldLabel: 'Product', name: "_product_name", readOnly: true, value: (appData)?appData.product_name:""
                        },{
                            fieldLabel: 'Loan #', name: "_loan_seq", readOnly: true, value: (appData)?appData.loan_seq:""
                        },{
                            fieldLabel: 'Channel', name: "_channel_name", readOnly: true, value: (appData)?appData.channel_name:""
                        }
                    ]
                }),
                Ext.create("component.abstract.GenericFieldContainer",{
                    columns: 2,
                    items:[
                        Ext.create('Ext.form.FieldSet', {
                            padding: '0 0 10 10',
                            title: "Requested Loan",
                            items:Ext.create("component.abstract.GenericFieldContainer",{
                                columns: 2,
                                items:[
                                    comboReqAmount,
                                    comboReqTerm,
                                    fieldTurnover
                                ]
                            })
                        }),
                        Ext.create('Ext.form.FieldSet', {
                            padding: '0 0 10 10',
                            title: "Approved Loan",
                            items: Ext.create("component.abstract.GenericFieldContainer",{
                                columns: 2,
                                items:[ comboApprovedAmount , comboApprovedTerm, fieldDeductionAmount,endOn ]
                            })
                        })
                    ]
                })
            ],
            listeners: {
                afterrender: function() {
                    if (fieldTurnover.isValid()) {
                        comboApprovedAmount.setDisabled(false);
                    }
                }
            }
        });
        this.callParent();

    }
});

