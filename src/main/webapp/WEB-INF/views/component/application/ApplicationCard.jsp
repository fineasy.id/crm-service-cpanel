<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.window.Window",
    layout: 'fit',
    width: 830,
    height: 650,
    onVerification: false,
    viewOnly: false,
    appData: null,
    initComponent: function() {

        var me = this;

        var form = Ext.create("component.application.ApplicationCardForm",{
            appData: me.appData,
            scrollable: true,
            parentWindow: me,
            gridStore: me.gridStore,
            viewOnly: me.viewOnly,
            onVerification: me.onVerification
        });

        var logData = Ext.create("component.application.ApplicationCardLog",{
            appData: me.appData,
            parentWindow: me,
            gridStore: me.gridStore,
            onVerification: me.onVerification
        });

        Ext.apply(this,{
            modal: true,
            title: 'Application Card #'+me.appData.app_id,
            items: [
                {
                    cls: 'panel-no-border',
                    border: false,

                    xtype: 'tabpanel', // TabPanel itself has no title
                    activeTab: 0,      // First tab active by default
                    items: [
                        form, logData
                    ]
                }
            ]
        });

        this.callParent();
    }
});