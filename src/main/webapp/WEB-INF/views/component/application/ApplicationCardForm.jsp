<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.form.Panel",
    defaultNoMr: null,
    title: "Application Detail",
    bodyStyle: 'background-color:#ffffff;',
    border: false,
    bodyPadding: 10,
    viewOnly: false,
    parentWindow: null,
    onVerification: false,
    appData: {},
    initComponent: function() {

        var me = this;
        var appData = me.appData;

        var fsApp = Ext.create('component.application.ApplicationCardFormLoan', {
            appData: me.appData,
            viewOnly: me.viewOnly,
            onVerification: me.onVerification
        });

        var fsEasypay = Ext.create('component.application.ApplicationCardFormEasypay', {
            appData: me.appData,
            viewOnly: me.viewOnly,
            onVerification: me.onVerification
        });

        var fsCustomer = Ext.create('component.application.ApplicationCardFormCustomer', {
            appData: me.appData,
            viewOnly: me.viewOnly,
            onVerification: me.onVerification
        });

        var topBar = Ext.create("component.application.ApplicationCardFormToolbar", {
            appData: me.appData,
            parentWindow: me.parentWindow,
            gridStore: me.gridStore,
            mainForm: me,
            onVerification: me.onVerification
        });

        Ext.apply(this,{
            waitMsgTarget: me.getId(),
            items: [
                fsCustomer,fsApp
            ],
            tbar: topBar
        });
        this.callParent();
    }
});