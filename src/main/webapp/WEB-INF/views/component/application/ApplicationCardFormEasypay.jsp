<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.form.FieldSet",
    title: "Easypay Account Detail",
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        readOnly: true
    },
    onVerification: false,
    appData: {},
    initComponent: function() {
        var me = this;
        var appData = me.appData;



        var fieldContainer = Ext.create("component.abstract.GenericFieldContainer",{
            items:[
                {
                    fieldLabel: 'EasyPay ID#',  name: "easypay_id", value: (appData)?appData.easypay_id:""
                }
            ]
        });

        Ext.apply(this,{
            items: [
                fieldContainer
            ]
        });
        this.callParent();
    }
});