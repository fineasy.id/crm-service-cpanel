<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

    Ext.define("${componentName}", {
        extend: "component.abstract.GenericTabGrid",
        layout:"fit",
        topBarMenuNew: false,
        storeFields:[
            { name: 'tmp_id',		type: 'number' },
            { name: 'first_name',		type: 'string' },
            { name: 'first_name',		type: 'string' },
            { name: 'first_name',		type: 'string' },
            { name: 'full_name',		type: 'string' },
            { name: 'mobile',		type: 'string' },
            { name: 'email',		type: 'string' },
            { name: 'reg_id',		type: 'string' },
            { name: 'birth_date',		type: 'date', dateFormat: 'Y-m-d' },
            { name: 'mother_name',		type: 'string' },
            { name: 'home_addr',		type: 'string' },
            { name: 'business_addr',		type: 'string' },
            { name: 'easypay_id',		type: 'string' },
            { name: 'loan_amount',		type: 'number' },
            { name: 'loan_term',		type: 'number' },
            { name: 'is_finished',		type: 'boolean' },
            { name: 'process_start',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'process_end',		type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'product_name',		type: 'string' },
            { name: 'marital_status_name',		type: 'string' },
            { name: 'children',		type: 'string' },
            { name: 'business_years',		type: 'string' },
            { name: 'loan_purpose',		type: 'string' },
            { name: 'ins_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'mod_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'status_name',		type: 'string' },
        ],
        storeUrl: "${dataProvider}/list.json",
        gridColumn: [{
            dataIndex: "tmp_id", text: "Gate ID", filter: { type: 'number'}, hidden: true, width: 90, align: 'right'
        },{
            dataIndex: "easypay_id", text: "Easypay ID",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "reg_id", text: "KTP ID",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "first_name", text: "First Name",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "mid_name", text: "Middle Name",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "last_name", text: "Last Name", filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "full_name", text: "Full Name", filter: {type: 'string'}, width: 200
        },{
            dataIndex: "mobile", text: "Mobile",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "email", text: "Email",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "birth_date", text: "Date of Birth",  filter: {type: 'string'}, width: 90, renderer: Ext.util.Format.dateRenderer('d M Y'),
        },{
            dataIndex: "mother_name", text: "Mother name",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "home_addr", text: "Home Address",  filter: {type: 'string'}, width: 200, hidden: true
        },{
            dataIndex: "business_addr", text: "Business Address",  filter: {type: 'string'}, width: 200, hidden: true
        },{
            dataIndex: "product_name", text: "Product Name",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "loan_amount", text: "Loan Amount",  filter: {type: 'number'}, width: 100, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },{
            dataIndex: "loan_term", text: "Loan Term",  filter: {type: 'number'}, width: 100, align: 'right'
        },{
            dataIndex: "marital_status_name", text: "Marital Status",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "children", text: "Chlidren #",  filter: {type: 'string'}, width: 90, hidden: true
        },{
            dataIndex: "is_finished", text: "Completed?",  filter: {type: 'boolean'}, width: 90, align: 'center', renderer: function(value) {
                if (value) return "Yes"; else return "No";
            }
        },{
            dataIndex: "loan_purpose", text: "Loan Purpose",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "business_years", text: "Years of Business",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "status_name", text: "Status",  filter: {type: 'string'}
        },{
            dataIndex: "ins_on", text: "Created On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
        },{
            dataIndex: "mod_on", text: "Modified On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
        }],
        windowFormClass: "component.admin.DataAdSlotForm",

        initComponent: function() {
            var me = this;
            Ext.apply(this,{
                viewConfig: {
                    getRowClass: function(record) {
                        // alert(record.data.date_end+" - "+new Date());

                    }
                },
            })
            this.callParent();
        }
    });
