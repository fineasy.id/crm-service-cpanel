<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: 'Ext.toolbar.Toolbar',
    onVerification: false,
    appData: {},
    initComponent: function() {
        var me = this;
        var appData = me.appData;
        var form = me.mainForm;


        Ext.apply(this,{

        })
        this.callParent();

        if (me.onVerification) {
            me.add('->');
            var buttonApprove = Ext.create('Ext.button.Button', {
                text: 'Approve',
                iconCls: 'accept',
                handler: function() {
                    if (!form.isValid()) return;

                    var onWindowClose = function() {
                        var baseForm = form.getForm();
                        Ext.Msg.confirm("Save","Are you sure to approve?", function(buttonId) {
                            if (buttonId != 'yes') return;
                            baseForm.submit({
                                waitMsg: 'Updating, please wait ...',
                                url: "<c:url value="/data/application/PhoneVerification/approve.json" />",
                                params: {
                                    "${_csrf.parameterName}": "${_csrf.token}",
                                    "contact_id": appData.contact_id,
                                    "app_id": appData.app_id
                                },
                                success: function(f,a,r) {
                                    Ext.toast({
                                        bodyStyle: 'background-color:#ffffff;',
                                        title: 'Application approved!',
                                        html: "Application approved successfully",
                                        iconCls: "tick",
                                        icon: Ext.Msg.ERROR,
                                        align: "t"
                                    });
                                    if (me.gridStore) me.gridStore.reload();
                                    me.parentWindow.close();
                                },
                                failure: function(f,a,r) {
                                    handleSubmitFailure(f,a);
                                }
                            });
                        })
                    }

                    openCustomerSurvey(appData.contact_id, onWindowClose);
                }
            });

            var buttonPending = Ext.create('Ext.button.Button', {
                text: 'Pending',
                iconCls: 'refresh',
                handler: function() {
                    var w = Ext.create("component.application.VerificationDecisionDialog", {
                        appData: appData,
                        title: 'Pending application #'+appData.app_id,
                        statusTag: 'pending',
                        parentWindow: me.parentWindow,
                        gridStore: me.gridStore,
                        onVerification: true
                    });
                    w.show();
                }
            });

            var buttonReject = Ext.create('Ext.button.Button', {
                text: 'Reject',
                iconCls: 'cancel',
                handler: function() {

                    var onWindowClose = function() {
                        var w = Ext.create("component.application.VerificationDecisionDialog", {
                            appData: appData,
                            title: 'REJECT application #'+appData.app_id,
                            statusTag: 'reject',
                            parentWindow: me.parentWindow,
                            gridStore: me.gridStore,
                            onVerification: true
                        });
                        w.show();
                    }
                    openCustomerSurvey(appData.contact_id, onWindowClose);
                }
            });

            me.add(buttonPending);
            me.add("-");
            me.add(buttonApprove);
            me.add(buttonReject);
        }
    }

});