<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.window.Window",
    appData: {},
    width: 400,
    onVerification: true,
    layout: 'fit',
    initComponent: function() {

        var me = this;
        var appData = me.appData;

        var comboStatus = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: me.statusTag.replace(/\b\w/g, function(l){ return l.toUpperCase() })+' reason',
            autoLoad: true,
            width: '90%',
            name: 'status_id',
            extraParams: {
                appId: appData.app_id,
                statusTag: me.statusTag,
            },
            url: "<c:url value="/data/misc/DataComboBox/applicationStatus.json" />",
            listeners: {
                change: function(c, newValue, oldValue, opts) {
                }
            }
        });

        var comboCallbackTime = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Callback after',
            autoLoad: true,
            name: 'callback_minute',
            value: "15",
            width: '90%',
            extraParams: {
                appId: appData.app_id
            },
            url: "<c:url value="/data/misc/DataComboBox/callbackTimeList.json" />",
            listeners: {
                change: function(c, newValue, oldValue, opts) {
                }
            }
        });

        var fieldNote = Ext.create("Ext.form.field.TextArea",{
            fieldLabel: 'Remarks',
            name: 'note',
            width: '90%',
            allowBlank: false
        })

        var form = Ext.create("Ext.form.Panel",{
            bodyStyle: 'background-color:#ffffff;',
            border: false,
            bodyPadding: 10,
            items: [
                comboStatus,
                fieldNote

            ]
        });

        if (me.statusTag == 'pending') {
            form.add(comboCallbackTime);
        }


        Ext.apply(this,{
            modal: true,
            items: [
                form
            ],
            tbar: {
                items: [
                    '->',
                    {
                        text: 'save',
                        iconCls: 'disk',
                        handler: function() {
                            if (!form.isValid()) return;
                            var baseForm = form.getForm();
                            Ext.Msg.confirm("Save","Are you sure to save?", function(buttonId) {
                                if (buttonId != 'yes') return;

                                baseForm.submit({
                                    waitMsg: 'Updating, please wait ...',
                                    url: "<c:url value="/data/application/PhoneVerification/" />"+me.statusTag+".json",
                                    params: {
                                        "${_csrf.parameterName}": "${_csrf.token}",
                                        app_id: appData.app_id,

                                    },
                                    success: function(f,a,r) {
                                        me.close();
                                        Ext.toast({
                                            bodyStyle: 'background-color:#ffffff;',
                                            title: 'Application updated',
                                            html: "Application updated successfully",
                                            iconCls: "tick",
                                            icon: Ext.Msg.ERROR,
                                            align: "t"
                                        })
                                        if (me.gridStore!= null)
                                            me.gridStore.reload();
                                        me.parentWindow.close();
                                        me.close();
                                    },
                                    failure: function(f,a,r) {
                                        handleSubmitFailure(f,a);
                                    }
                                })
                            });
                        }
                    }
                ]
            }
        });

        this.callParent();
    }
});