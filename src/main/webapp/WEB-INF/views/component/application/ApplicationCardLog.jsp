<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "component.abstract.GenericTabGrid",
    layout: "fit",
    title: "Application Log",
    storeSorters: [{
        property: "ins_on",
        direction: "ASC"
    }],
    topBarMenuNew: false,
    appData: {},
    storeFields:[
        { name: 'app_id',		type: 'number' },
        { name: 'ins_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'ins_by',		type: 'number' },
        { name: 'status_id',		type: 'number' },
        { name: 'callback_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'note',		type: 'string' },
        { name: 'status_name',		type: 'string' },
        { name: 'created_by',		type: 'string' }
    ],
    storeUrl: "${dataProvider}/list.json",
    gridColumn: [
        {
            dataIndex: "app_id", text: "Application ID", filter: { type: 'number'}, width: 90, align: 'right', hidden: true
        },{
            dataIndex: "ins_on", text: "Updated On",  filter: { type: 'date', dateFormat: 'Y-m-d' }, width: 90, renderer: Ext.util.Format.dateRenderer('d M Y H:i')
        },{
            dataIndex: "created_by", text: "Updated By",  filter: {type: 'string'}, width: 150
        },{
            dataIndex: "status_name", text: "Update Status",  filter: {type: 'string'}, width: 150
        },{
            dataIndex: "note", text: "Remarks",  filter: {type: 'string'}, width: 150
        },{
            dataIndex: "callback_on", text: "Callback On",  filter: { type: 'date', dateFormat: 'Y-m-d' }, width: 90, renderer: Ext.util.Format.dateRenderer('d M Y H:i')
        }
    ],
    initComponent: function () {
        var me = this;

        me.extraParams["app_id"] = me.appData.app_id;
        Ext.apply(this,{
            viewConfig: {
                getRowClass: function(record) {
                    // alert(record.data.date_end+" - "+new Date());

                }
            },
        });
        this.callParent();
    }
});