<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.form.FieldSet",
    title: "Customer Detail",
    defaultType: 'textfield',
    padding: '0 0 10 10',
    viewOnly: false,
    defaults: {
        allowBlank: false,
        readOnly: true
    },
    onVerification: false,
    appData: {},
    initComponent: function() {
        var me = this;
        var appData = me.appData;

        var allowChange = (me.onVerification)?false:true;

        var defaultListeners = {
            change: function(f) {
               f.changed = true;
            },
            blur: function(f) {
                if (!f.changed) return;
                if (!f.isValid()) return;
                if (f.getValue()=="") return;
                var fieldName = f.getName();
                var fieldValue = f.getValue();
                var url = '<c:url value="/data/application/PhoneVerification/updateContact.json" />';
                f.changed = false;
                updateField(f, url, fieldName, fieldValue)
            }
        }

        var applicationListeners = {
            change: function(f) {
                f.changed = true;
            },
            blur: function(f) {
                if (!f.changed) return;
                if (!f.isValid()) return;
                if (f.getValue()=="") return;
                var fieldName = f.getName();
                var fieldValue = f.getValue();
                var url = '<c:url value="/data/application/PhoneVerification/updateApplication.json" />';
                f.changed = false;
                updateField(f, url, fieldName, fieldValue)
            }
        }

        var updateField = function(field, url, fieldName, fieldValue) {
            Ext.Ajax.request({
                url: url,
                method: 'POST',
                params: {
                    appId: appData.app_id,
                    contactId: appData.contact_id,
                    fieldName: fieldName,
                    fieldValue: fieldValue,
                    "${_csrf.parameterName}": "${_csrf.token}"
                },
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                    if (!obj.success) {
                        Ext.Msg.alert('Alert', obj.msg, function(){
                            field.reset();
                        });
                    }
                },
                failure: function(transport){
                    alert("Error: " - transport.responseText);
                }
            });
        }

        var comboEducation = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Education',
            autoLoad: true,
            name: "education_id",
            value: (appData.education_id)?appData.education_id:"",
            readOnly: allowChange,
            url: "<c:url value="/data/misc/DataComboBox/educationList.json" />",
            listeners: defaultListeners
        });

        var comboMarital = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Marital Status',
            autoLoad: true,
            name: "marital_status_id",
            value: (appData.marital_status_id)?appData.marital_status_id:"",
            readOnly: allowChange,
            url: "<c:url value="/data/misc/DataComboBox/maritalList.json" />",
            listeners: defaultListeners
        });

        var comboChildren = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Children',
            autoLoad: true,
            name: "children_id",
            value: (appData.children_id)?appData.children_id:"",
            readOnly: allowChange,
            url: "<c:url value="/data/misc/DataComboBox/childrenList.json" />",
            listeners: defaultListeners
        });
        var comboBusinessYears = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Business Years',
            autoLoad: true,
            name: "business_years_id",
            value: (appData.business_years_id)?appData.business_years_id:"",
            readOnly: allowChange,
            url: "<c:url value="/data/misc/DataComboBox/businessYearsList.json" />",
            listeners: defaultListeners
        });

        var comboLoanPurpose = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Loan purpose',
            autoLoad: true,
            name: "purpose_id",
            value: (appData.purpose_id)?appData.purpose_id:"",
            readOnly: allowChange,
            url: "<c:url value="/data/misc/DataComboBox/loanPurposeList.json" />",
            listeners: applicationListeners
        });

        var initDate = new Date();
        initDate.setFullYear( initDate.getFullYear() - 17 );

        Ext.apply(this,{
            items: [
                Ext.create("component.abstract.GenericFieldContainer",{
                    items:[
                        {
                            fieldLabel: 'Full Name', name: "full_name", readOnly: allowChange, value: (appData)?appData.full_name:"",listeners: defaultListeners
                        },{
                            fieldLabel: 'Mobile#', name: "mobile", readOnly: true, value: (appData)?appData.mobile:""
                        },{
                            fieldLabel: 'EasyPay ID#',  name: "easypay_id", value: (appData)?appData.easypay_id:"", readOnly: true
                        },{
                            fieldLabel: 'Email address', name: "email", readOnly: allowChange, value: (appData)?appData.email:"",listeners: defaultListeners, vtype: 'email', maskRe: /[a-z0-9@\._-]/,
                        },{
                            fieldLabel: 'KTP',  name: "reg_id", value: (appData)?appData.reg_id:"", readOnly: allowChange, listeners: defaultListeners, maskRe: /[0-9]/
                        },{
                            xtype: 'datefield',
                            anchor: '100%',
                            readOnly: allowChange,
                            format: 'Y-m-d',
                            name: "birth_date",
                            fieldLabel: 'Date of birth',
                            value: (appData)?appData.birth_date:initDate,
                            maxValue: initDate  // limited to the current date or prior
                            , listeners: defaultListeners
                        }
                        ,comboEducation
                        ,comboMarital
                        ,comboChildren
                        ,{
                            fieldLabel: 'Mother maiden name', name: "mother_name", readOnly: allowChange, value: (appData)?appData.mother_name:"", listeners: defaultListeners
                        }
                        ,comboBusinessYears
                        ,comboLoanPurpose
                        ,{
                            fieldLabel: 'Home Address', name: "home_addr", readOnly: false, value: (appData)?appData.home_addr:"", width: '80%', colspan: 2, listeners: defaultListeners, readOnly: (me.viewOnly)?true:false
                        }, {
                            fieldLabel: 'Business Address', name: "business_addr", readOnly: false, value: (appData)?appData.business_addr:"", colspan: 2, layout: 'fit', width: '80%', listeners: defaultListeners, readOnly:  (me.viewOnly)?true:false
                        }
                    ]
                })
            ]
        });
        this.callParent();
    }
});

