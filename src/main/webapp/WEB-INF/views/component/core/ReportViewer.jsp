<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

Ext.define("${componentName}", {	
	extend: "Ext.window.Window",
	width: 800,
	height: 600,	
	title: "Report Viewer",	
	url: null,
	initComponent: function() {
		var me = this;
		Ext.apply(this, {
    		items: [{
    	        xtype : "component",
    	        autoEl : {
    	            tag : "iframe",
    	            style: 'height: 100%; width: 100%; border: none',
    	            src : me.url
    	        }
    	    }]
    	})
    	
    	this.callParent();
	}
});