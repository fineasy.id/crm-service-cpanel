<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />
//<script>
Ext.tip.QuickTipManager.init();
Ext.create('Ext.container.Viewport', {
	layout: 'border',
	id: "main-viewport",
	padding: '5 5 5 5',
	items:[

		Ext.create('Ext.panel.Panel',{
		    id: "main-body-panel",
			layout: "border",
            region: "center",
            items: [
				{
				    region: "center",
				    layout:"fit",
				    tbar: Ext.create('component.core.MainToolbar', {}),
                    padding: '5 5 5 0',
				    items:{
				        id: "maintab-panel",
				        cls: 'panel-no-border',
				        border: false,
				        xtype: 'tabpanel', // TabPanel itself has no title
				        activeTab: 0,      // First tab active by default
				        items: [
				            //Ext.create('component.core.Dashboard', {})
				        ]
				    },
				    listeners: {
				        render: function(p, o) {
				            //Ext.Msg.alert("YES","OK deh kk");
				            //Ext.get('loading-div').hide();
				            $("#loading-div").fadeOut(2000,function() {
				                // Animation complete.
				            });
                            //openMainTab(103);
				        },
                        afterrender: function(p,o) {

                        }
				    }
				},
                Ext.create('component.core.MainMenu',{

                })
            ]
		})
	]
});

