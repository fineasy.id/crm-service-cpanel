<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

Ext.define("${componentName}", {
	extend: 'Ext.panel.Panel',
	padding: 5,
	layout: {
        type: 'table',
        columns: 3,
        tdAttrs: { style: 'padding: 5px 5px 10px 5px;' }
    },
    defaults: {
        bodyPadding: 20,
        height: 200,
        border: true
    },
	title: "Dashboard",
	border: false,
	initComponent: function() {
		var me = this;
		
		Ext.apply(this,{
			items: []
		})
		
		this.callParent();
	}
});