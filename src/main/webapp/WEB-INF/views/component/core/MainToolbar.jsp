<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />
//<script>

	Ext.define("${componentName}", {
        extend: 'Ext.toolbar.Toolbar',
        items:[
            '->',
            {
                text: 'Profile',
                iconCls: 'user',
                menu:[
                    {
                        text: "Edit profile",
                        iconCls: 'disk',
                        handler: function() {
                        }
                    },
                    '-',
                    {
                        text: 'Logout',
                        iconCls: 'door_open',
                        listeners: {
                            click: function() {
                                Ext.Msg.confirm("Logout","Are you sure to logout?", function(buttonId) {
                                    if (buttonId == 'yes') {
                                        window.location.replace("<c:url value="/logout" />");
                                    }
                                })
                            }
                        }
                    }
                ]
            }
        ]
    });