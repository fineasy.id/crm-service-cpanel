<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />
//<script>
    /**
     * Created by Indra on 14/04/2015.
     */
    renderRowLink = function(v,p,r) {
        var html = "<a xaction='edit' href='javascript:void(0);'>"+v+"</a>";
        return html;
    }

    randInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    showBodyMask = function() {
        Ext.getBody().mask('Loading please wait ....');
    }

    ajaxRequest = function(obj) {
        var params = obj.params;
        params["${_csrf.parameterName}"] = "${_csrf.token}";
        Ext.getBody().mask('Loading please wait ....');
        Ext.Ajax.request({
            url: obj.url,
            method: 'POST',
            params: params,
            success: function(response){
                var o = Ext.decode(response.responseText);
                if (o.success) {
                    if(obj["onSuccess"]) obj["onSuccess"](o);
                } else {
                    Ext.getBody().unmask();
                    Ext.Msg.alert('Failed', o.msg, function(){
                    });
                }
            },
            failure: function(response) {
                Ext.getBody().unmask();
                var o = Ext.decode(response.responseText);
                Ext.Msg.alert('Failed', o.msg, function(){
                });
            }
        });
    }

    openMainTab = function(module) {
        //console.log("Module: "+module.menu_id);
        //console.log("module.name_view: "+module.name_view)
        var tabId = "main-tab-"+module.menu_id;
        var tab = Ext.getCmp(tabId);
        var mainTab = Ext.getCmp('maintab-panel')
        if (tab == null) {

            Ext.getBody().mask('Loading please wait ....');
            tab = Ext.create(module.name_view, {
                title: module.text,
                iconCls: module.iconCls,
                closable: true,
                border: false,
                id: tabId,
                listeners: {
                    afterrender: function() {
                        Ext.getBody().unmask();
                    }
                }
            });
            mainTab.add(tab);
        }
        mainTab.setActiveTab(tab);
    }

    handleSubmitFailure = function(f,a) {
        switch (a.failureType) {
            case Ext.form.action.Action.CLIENT_INVALID:
                Ext.Msg.alert({
                    icon: Ext.Msg.ERROR,
                    title: "Failure when submitting",
                    message: "Form fields may not be submitted with invalid values"
                });
                break;
            case Ext.form.action.Action.CONNECT_FAILURE:
                Ext.Msg.alert({
                    icon: Ext.Msg.ERROR,
                    title: "Network Problem",
                    message: "Network problem while update data!<br/>Please check your internet connection or contact your system administrator!"
                });
                break;
            case Ext.form.action.Action.SERVER_INVALID:
                Ext.Msg.alert({
                    icon: Ext.Msg.ERROR,
                    title: "Failure when submitting data",
                    message: (a.result.msg)?a.result.msg:"Unknown error!"
                });
        }
    }

    nl2br = function (str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        var newStr = (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        console.log(newStr);
        return newStr;
    }

    showToast = function(obj) {
        Ext.toast({
            bodyStyle: 'background-color:#ffffff;',
            title: obj.title,
            html: obj.text,
            iconCls: "tick",
            icon: Ext.Msg.ERROR,
            align: "t"
        });
    }

    openContractDetail = function(contractId, gridStore) {
        Ext.getBody().mask('Loading please wait ....');
        Ext.Ajax.request({
            url: '<c:url value="/data/contract/ContractList/detail.json" />',
            method: 'POST',
            params: {
                "contractId": contractId,
                "${_csrf.parameterName}": "${_csrf.token}"
            },
            success: function(response){

                var obj = Ext.decode(response.responseText);
                if (obj.success) {
                    var w = Ext.create("component.contract.ContractCard", {
                        formData: obj.data,
                        deskCollection: false,
                        gridStore: gridStore,
                        listeners: {
                            afterrender: function(w) { Ext.getBody().unmask(); }
                        }
                    });
                    w.show();
                } else {
                    Ext.getBody().unmask();
                    Ext.Msg.alert('No application', obj.msg, function(){

                    });
                }
            },
            failure: function(transport){
                Ext.getBody().unmask();
                alert("Error: " - transport.responseText);
            }
        });
    }

    openApplicationDetail = function(appId, gridStore, viewOnly) {
        Ext.getBody().mask('Loading please wait ....');
        Ext.Ajax.request({
            url: '<c:url value="/data/application/ApplicationList/detail.json" />',
            method: 'POST',
            params: {
                "appId": appId,
                "${_csrf.parameterName}": "${_csrf.token}"
            },
            success: function(response){
                var obj = Ext.decode(response.responseText);
                if (obj.success) {
                    var w = Ext.create("component.application.ApplicationCard", {
                        appData: obj.data,
                        onVerification: false,
                        viewOnly: viewOnly,
                        gridStore: gridStore,
                        listeners: {
                            afterrender: function(w) {
                                Ext.getBody().unmask();
                            }
                        }
                    });
                    w.show();
                } else {
                    Ext.getBody().unmask();
                    Ext.Msg.alert('No application', obj.msg, function(){

                    });
                }
            },
            failure: function(transport){
                Ext.getBody().unmask();
                alert("Error: " - transport.responseText);
            }
        });
    }

    var newApplication = function(isRepeat) {
        if (!isRepeat) isRepeat = false;
        Ext.Msg.prompt('EASYPAY ID / Mobile number', '<div style="margin-bottom:10px;">Please enter customer\'s Easypay ID or mobile phone number!<br/>Or leave it blank to get available leads!</div>', function(btn, text) {
            if (btn == 'ok') {
                Ext.getBody().mask('Loading please wait ....');
                Ext.Ajax.request({
                    url: '<c:url value="/data/application/Telesales/newApplication.json" />',
                    method: 'POST',
                    params: {
                        "isRepeat": isRepeat,
                        "searchId": text.trim(),
                        "${_csrf.parameterName}": "${_csrf.token}"
                    },
                    success: function(response){
                        var obj = Ext.decode(response.responseText);
                        Ext.getBody().unmask();
                        if (obj.success) {
                            if (obj.hasUnprocessed) {
                                Ext.Msg.alert('Unfinished Application!', obj.msg, function(){
                                    globalFunction["getPhoneVerification"]();
                                });
                            } else {
                                globalFunction["getPhoneVerification"]();
                            }
                        }
                    },
                    failure: function(response){
                        Ext.getBody().unmask();
                        var obj = Ext.decode(response.responseText);
                        Ext.Msg.alert('Failed!', obj.msg, function(){
                        });
                    }
                });
            }
        });
    }

    var phoneVerification = function(isRepeat) {
        if (!isRepeat) isRepeat = false;
        Ext.getBody().mask('Loading please wait ....');
        Ext.Ajax.request({
            url: '<c:url value="/data/application/PhoneVerification/getUnverifiedApplication.json" />',
            method: 'POST',
            params: {
                isRepeat: isRepeat,
                "${_csrf.parameterName}": "${_csrf.token}"
            },
            success: function(response){
                var openPvWindow = function(row) {
                    var w = Ext.create("component.application.ApplicationCard", {
                        appData: row,
                        onVerification: true,
                        gridStore: Ext.get('application-list-store'),
                        listeners: {
                            afterrender: function(w) {
                                Ext.getBody().unmask();
                            }
                        }

                    });
                    w.show();
                }
                var obj = Ext.decode(response.responseText);
                if (obj.success) {
                    if (obj.msg) {
                        Ext.Msg.alert('Alert', obj.msg, function(){
                            openPvWindow(obj.row);
                        });
                    } else openPvWindow(obj.row);
                } else {
                    Ext.getBody().unmask();
                    Ext.Msg.alert('No application', obj.msg, function(){
                    });

                }
            },
            failure: function(response){
                Ext.getBody().unmask();
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('No application', obj.msg, function(){
                });

            }
        });
    }

    openCustomerSurvey = function(contactId, onClose) {
        Ext.Ajax.request({
            url: '<c:url value="/data/misc/CustomerSurvey/list.json" />',
            method: 'POST',
            params: {
                contactId: contactId,
                "${_csrf.parameterName}": "${_csrf.token}"
            },
            success: function(response){
                var obj = Ext.decode(response.responseText);
                if (obj.success) {
                    var w = Ext.create("component.misc.CustomerSurvey", {
                        surveyItems: obj.items,
                        surveyId: obj.id,
                        contactId: contactId,
                        listeners: {
                            close: function(w) {
                                onClose();
                            }
                        }
                    });
                    w.show();
                } else {
                    onClose();
                }

            },
            failure: function(response){
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Failed!', obj.msg, function(){
                });
            }
        });
    }

    globalFunction = {
        openTelesalesWindow: function (gridStore) {
            Ext.getBody().mask('Loading please wait ....');
            Ext.Ajax.request({
                url: '<c:url value="/data/application/Telesales/getQueue.json" />',
                method: 'POST',
                params: {
                    "${_csrf.parameterName}": "${_csrf.token}"
                },
                success: function(response){

                    var obj = Ext.decode(response.responseText);
                    if (obj.success) {
                        var w = Ext.create("component.application.ApplicationCard", {
                            appData: obj.row,
                            onVerification: false,
                            gridStore: gridStore,
                            listeners: {
                                afterrender: function(w) {
                                    Ext.getBody().unmask();
                                }
                            }
                        });
                        w.show();
                    } else {
                        Ext.getBody().unmask();
                        Ext.Msg.alert('No application', obj.msg, function(){
                        });
                    }
                },
                failure: function(transport){
                    alert("Error: " - transport.responseText);
                    Ext.getBody().unmask();
                }
            });
        },
        getPhoneVerification: function() {
            phoneVerification(false);
        },
        getPhoneVerificationRepeat: function() {
            phoneVerification(true);
        },
        createNewApplication: function () {
            newApplication(false);
        },
        createRepeatApplication: function() {
            newApplication(true);
        },
        openSurvey: function() {
            Ext.Ajax.request({
                url: '<c:url value="/data/misc/CustomerSurvey/list.json" />',
                method: 'POST',
                params: {
                    "${_csrf.parameterName}": "${_csrf.token}"
                },
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                    var w = Ext.create("component.misc.CustomerSurvey", {
                        surveyItems: obj.items,
                        surveyId: obj.id
                    });
                    w.show();
                },
                failure: function(response){
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Failed!', obj.msg, function(){
                    });
                }
            });
        },
        getDeskCollection: function() {
            Ext.getBody().mask('Loading please wait ....');
            Ext.Ajax.request({
                url: '<c:url value="/data/collection/DeskCollection/getQueue.json" />',
                method: 'POST',
                params: {
                    "${_csrf.parameterName}": "${_csrf.token}"
                },
                success: function(response){

                    var obj = Ext.decode(response.responseText);
                    if (obj.success) {
                        var w = Ext.create("component.contract.ContractCard", {
                            formData: obj.data,
                            deskCollection: true,
                            gridStore: null,
                            listeners: {
                                afterrender: function(w) {
                                    Ext.getBody().unmask();
                                }
                            }
                        });
                        w.show();
                    } else {
                        Ext.getBody().unmask();
                        Ext.Msg.alert('No application', obj.msg, function(){
                        });
                    }
                },
                failure: function(transport){
                    Ext.getBody().unmask();
                    alert("Error: " - transport.responseText);
                }
            });
        }
    };



