<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />
//<script>
$( document ).ready(function(){


    jQuery.cachedScript = function( url, options ) {

        // Allow user to set any option except for dataType, cache, and url
        options = $.extend( options || {}, {
            dataType: "script",
            cache: true,
            async: true,
            url: url
        });

        // Use $.ajax() since it is more flexible than $.getScript
        // Return the jqXHR object so we can chain callbacks
        return jQuery.ajax( options );
    };

    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', contextPath+'/ext/css/'+extTheme+'/theme-'+extTheme+'-all.css') );
    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', contextPath+'/ext/css/charts-all.css') );
    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', contextPath+'/css/icon.css') );    
    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', contextPath+'/css/fullcalendar.min.css') );    
    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', contextPath+'/css/jquery.qtip.min.css') );

    $.cachedScript(contextPath+"/ext/ext-all.js").done(function( script, textStatus ) {
    	Ext.onReady(function() {
    		Ext.tip.QuickTipManager.init();
            $.cachedScript("<c:url value="/component/core/Custom.js" />").done(function( script, textStatus ) {
                Ext.require("component.core.MainBody");
            });
        });
	});
});

generateUUID = function() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

mergeObjects = function(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

// _data_province = ${dataProvince};
// _data_city = ${dataCity};
