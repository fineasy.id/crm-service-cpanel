<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />
//<script type="text/javascript">

	Ext.define("${componentName}", {
        extend: 'Ext.panel.Panel',
        layout: 'accordion',
        collapsible:true,
        title: "FINEASY - CRM",
        width: 250,
        padding: '5 5 5 5',
        region: 'west',
        initComponent: function() {
            Ext.apply(this,${menuItems})
            this.callParent();
        }
    });

Ext.define("component.core.MainMenuPanel", {
	alias: "widget.mainmenupanel",
	border:false,
    layout:"fit",
    menuItems: null,
    extend: 'Ext.tree.TreePanel',
    initComponent: function() {
    	var me = this;
    	Ext.apply(this,{
    		rootVisible: false,
    		store: {
				root: {
                    text: '-',
                    expanded: true,
                	children: me.menuItems
				}
			},
			listeners: {
                cellclick: function(tp, td, cellIndex, record, tr, rowIndex, e, eOpts){
                    if (!record.data.leaf) return;
                    if (record.data.action_type=='maintab') {
                        openMainTab(record.data);
                    } else if (record.data.action_type=='function') {
                        globalFunction[record.data.name_view]();
                    }
                }
            }
    	});
    	this.callParent();
    }
});

/**
Ext.define('component.main.MainMenuPanel', {
    alias: "widget.mainmenupanel",
    title: "Yess",
    border:false,
    layout:"fit",
    menuItems: null,
    extend: 'Ext.panel.Panel',
    initComponent: function() {
        var me = this;
        Ext.apply(this,{
            items: {
                xtype: 'treepanel',
                border:0,
                rootVisible: false,
                store: {
                    root: {
                        text: '-',
                        expanded: true,
                        children: me.menuItems
                    }
                },
                listeners: {
                    cellclick: function(tp, td, cellIndex, record, tr, rowIndex, e, eOpts){
                        if (!record.data.leaf) return;
                        if (record.data.action_type=='maintab') {
                            openMainTab(record.data.menu_id, record.data.action_module);
                        }
                    }
                }
            }
        })
        this.callParent();
    }
});
**/
