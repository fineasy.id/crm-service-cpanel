<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.window.Window",
    formData: {},
    width: 400,
    successResult: 1,
    parentWindow: null,
    layout: 'fit',
    initComponent: function() {

        var me = this;
        var formData = me.formData;

        var comboStatus = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Result',
            autoLoad: true,
            width: '90%',
            name: 'resultId',
            extraParams: {
                contractId: formData.contract_id,
                successResult: me.successResult,
            },
            url: "<c:url value="/data/misc/DataComboBox/collectionCallResult.json" />"
        });

        var fieldNote = Ext.create("Ext.form.field.TextArea",{
            fieldLabel: 'Remarks',
            name: 'note',
            width: '100%',
            allowBlank: false
        })

        var form = Ext.create("Ext.form.Panel",{
            bodyStyle: 'background-color:#ffffff;',
            border: false,
            bodyPadding: 10,
            items: [
                comboStatus,
                fieldNote
            ]
        });

        Ext.apply(this,{
            modal: true,
            items: [
                form
            ],
            tbar: {
                items: [
                    '->',
                    {
                        text: 'Save',
                        iconCls: 'disk',
                        handler: function() {
                            var baseForm = form.getForm();
                            if (!form.isValid()) return;
                            Ext.Msg.confirm("Save","Are you sure to save?", function(buttonId) {
                                if (buttonId != 'yes') return;

                                baseForm.submit({
                                    waitMsg: 'Updating, please wait ...',
                                    url: "<c:url value="/data/collection/DeskCollection/updateCall.json" />",
                                    params: {
                                        "${_csrf.parameterName}": "${_csrf.token}",
                                        contractId: formData.contract_id,
                                        successResult: me.successResult
                                    },
                                    success: function(f,a,r) {
                                        Ext.toast({
                                            bodyStyle: 'background-color:#ffffff;',
                                            title: 'Call result updated',
                                            html: "Call result updated successfully",
                                            iconCls: "tick",
                                            icon: Ext.Msg.ERROR,
                                            align: "t"
                                        });
                                        me.parentWindow.close();
                                        me.close();
                                    },
                                    failure: function(f,a,r) {
                                        handleSubmitFailure(f,a);
                                    }
                                })
                            });

                        }
                    }
                ]
            }
        });

        this.callParent();
    }
});