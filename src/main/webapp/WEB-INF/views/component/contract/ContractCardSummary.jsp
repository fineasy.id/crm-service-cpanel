<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;
Ext.define("${componentName}", {
    extend: "component.abstract.GenericTabGrid",
    layout:"fit",
    topBarMenuNew: false,
    contractId: null,
    storeFields:[
        { name: 'id',		type: 'number' },
        { name: 'contract_id',		type: 'number' },
        { name: 'bold',		type: 'number' },
        { name: 'name',		type: 'string' },
        { name: 'value_formatted',		type: 'string' },
        { name: 'ins_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'mod_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' }
    ],
    storeUrl: "${dataProvider}/list.json",
    storeSorters: [{
        property: "display_order",
        direction: "ASC"
    }],
    gridColumn: [
        {
            dataIndex: "name", text: "Description",  filter: {type: 'string'}, width: 400
        },
        {
            dataIndex: "value_formatted", text: "Value",  filter: {type: 'string'}, width: 250
        },
        {
            dataIndex: "mod_on", text: "Modified On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }, hidden: true
        }
    ],
    initComponent: function() {
        var me = this;

        Ext.apply(this,{
            extraParams: {
                filtered: true,
                contractId: me.contractId
            },
            viewConfig: {
                getRowClass: function(record) {
                    // alert(record.data.date_end+" - "+new Date());

                }
            },
        });
        this.callParent();
    }

});