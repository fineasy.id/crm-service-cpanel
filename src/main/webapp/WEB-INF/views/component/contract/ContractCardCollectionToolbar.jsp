<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: 'Ext.toolbar.Toolbar',
    parentWindow: null,
    formData: {},
    initComponent: function() {
        var me = this;
        var formData = me.formData;
        var buttonSuccess = Ext.create('Ext.button.Button', {
            text: 'Success',
            iconCls: 'accept',
            handler: function() {
                var w = Ext.create("component.contract.ContractCardCollectionDialog", {
                    formData: formData,
                    title: 'Call success result on contract #'+formData.contract_id,
                    successResult: 1,
                    parentWindow: me.parentWindow
                });
                w.show();
            }
        });

        var buttonFailed = Ext.create('Ext.button.Button', {
            text: 'Failed',
            iconCls: 'delete',
            handler: function() {
                Ext.Msg.confirm("Save","Call failed?", function(buttonId) {
                    if (buttonId != 'yes') return;
                    Ext.Ajax.request({
                        url: '<c:url value="/data/collection/DeskCollection/updateCall.json" />',
                        method: 'POST',
                        params: {
                            contractId: formData.contract_id,
                            successResult: 0,
                            "${_csrf.parameterName}": "${_csrf.token}"
                        },
                        success: function(response){

                            var obj = Ext.decode(response.responseText);
                            if (obj.success) {
                                me.parentWindow.close();
                                showToast({
                                    title: "Call result updated",
                                    text: "Call result updated successfully"
                                })
                            } else {
                                Ext.Msg.alert('Error', obj.msg, function(){
                                });
                            }
                        },
                        failure: function(transport){
                            alert("Error: " - transport.responseText);
                        }
                    });
                });
            }
        });

        Ext.apply(this,{
            items:[
                '->','Desk collection call result',
                '-',
                buttonSuccess,
                buttonFailed
            ]
        })
        this.callParent();
    }
});