<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "component.abstract.GenericTabGrid",
    layout:"fit",
    topBarMenuNew: false,
    contractId: null,
    storeFields:[
        { name: 'id',		type: 'number' },
        { name: 'contract_id',		type: 'number' },
        { name: 'ins_by_name',		type: 'string' },
        { name: 'result_name',		type: 'string' },
        { name: 'remark',		type: 'string' },
        { name: 'ins_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'mod_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' }
    ],
    storeUrl: "<c:url value="/data/collection/DeskCollection/list.json" />",
    storeSorters: [{
        property: "ins_on",
        direction: "ASC"
    }],
    gridColumn: [
        {
            dataIndex: "ins_on", text: "Created On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 150, filter: { type: 'date', dateFormat: 'Y-m-d' }
        },
        {
            dataIndex: "ins_by_name", text: "Operator name",  filter: {type: 'string'}, width: 200
        },
        {
            dataIndex: "result_name", text: "Result",  filter: {type: 'string'}, width: 200
        },
        {
            dataIndex: "remark", text: "Remark",  filter: {type: 'string'}, width: 300
        }
    ],
    initComponent: function() {
        var me = this;

        Ext.apply(this,{
            extraParams: {
                filtered: true,
                contractId: me.contractId
            },
            viewConfig: {
                getRowClass: function(record) {
                    // alert(record.data.date_end+" - "+new Date());

                }
            },
        });
        this.callParent();
    }
});