<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

    Ext.define("${componentName}", {
        extend: "component.abstract.GenericTabGrid",
        layout:"fit",
        topBarMenuNew: false,
        storeFields:[
            { name: 'contract_id',		type: 'number' },
            { name: 'app_id',		type: 'number' },
            { name: 'first_name',		type: 'string' },
            { name: 'first_name',		type: 'string' },
            { name: 'first_name',		type: 'string' },
            { name: 'full_name',		type: 'string' },
            { name: 'mobile',		type: 'string' },
            { name: 'email',		type: 'string' },
            { name: 'reg_id',		type: 'string' },
            { name: 'birth_date',		type: 'date', dateFormat: 'Y-m-d' },
            { name: 'mother_name',		type: 'string' },
            { name: 'home_addr',		type: 'string' },
            { name: 'business_addr',		type: 'string' },
            { name: 'easypay_id',		type: 'string' },
            { name: 'amount_approved',		type: 'number' },
            { name: 'term_approved',		type: 'number' },
            { name: 'daily_charge',		type: 'number' },
            { name: 'contract_start_on',		type: 'date', dateFormat: 'Y-m-d' },
            { name: 'contract_charged_on',		type: 'date', dateFormat: 'Y-m-d' },
            { name: 'contract_end_on',		type: 'date', dateFormat: 'Y-m-d' },
            { name: 'is_finished',		type: 'boolean' },
            { name: 'process_start',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'process_end',		type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'product_name',		type: 'string' },
            { name: 'marital_status_name',		type: 'string' },
            { name: 'children',		type: 'string' },
            { name: 'business_years',		type: 'string' },
            { name: 'loan_purpose',		type: 'string' },
            { name: 'ins_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'mod_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'callback_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'status_name',		type: 'string' },
        ],
        storeUrl: "${dataProvider}/list.json",
        gridColumn: [{
            xtype:'actioncolumn',
            width:50,
            align: 'center',
            items: [{
                iconCls: 'edit',
                tooltip: 'View',
                handler: function(grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    openContractDetail(rec.get('contract_id'), grid.getStore());
                }
            }]
        },{
            dataIndex: "contract_id", text: "Contract ID", filter: { type: 'number'}, width: 90, align: 'right'
        },{
            dataIndex: "ins_on", text: "Created On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
        },{
            dataIndex: "mod_on", text: "Modified On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
        },{
            dataIndex: "status_name", text: "Status",  filter: {type: 'string'}
        },{
            dataIndex: "app_id", text: "Application ID", filter: { type: 'number'}, width: 90, align: 'right'
        },{
            dataIndex: "easypay_id", text: "Easypay ID",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "reg_id", text: "KTP ID",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "first_name", text: "First Name",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "mid_name", text: "Middle Name",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "last_name", text: "Last Name", filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "full_name", text: "Full Name", filter: {type: 'string'}, width: 200
        },{
            dataIndex: "mobile", text: "Mobile",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "email", text: "Email",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "birth_date", text: "Date of Birth", hidden:true, filter: {type: 'string'}, width: 90, renderer: Ext.util.Format.dateRenderer('d M Y'),
        },{
            dataIndex: "mother_name", text: "Mother name",  filter: {type: 'string'}, width: 100, hidden: true
        },{
            dataIndex: "home_addr", text: "Home Address",  filter: {type: 'string'}, width: 200, hidden: true
        },{
            dataIndex: "business_addr", text: "Business Address",  filter: {type: 'string'}, width: 200, hidden: true
        },{
            dataIndex: "product_name", text: "Product Name",  filter: {type: 'string'}, width: 100
        },{
            dataIndex: "amount_approved", text: "Loan Amount",  filter: {type: 'number'}, width: 100, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },{
            dataIndex: "term_approved", text: "Loan Term",  filter: {type: 'number'}, width: 100, align: 'right'
        },{
            dataIndex: "daily_charge", text: "Daily Deduction",  filter: {type: 'number'}, width: 100, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },{
            dataIndex: "contract_start_on", text: "Start On",  filter: {type: 'date', dateFormat: 'Y-m-d'}, width: 90, renderer: Ext.util.Format.dateRenderer('d M Y'),
        },{
            dataIndex: "contract_charged_on", text: "Start Deduction",  filter: {type: 'date', dateFormat: 'Y-m-d'}, width: 90, renderer: Ext.util.Format.dateRenderer('d M Y'),
        },{
            dataIndex: "contract_end_on", text: "End On",  filter: {type: 'date', dateFormat: 'Y-m-d'}, width: 90, renderer: Ext.util.Format.dateRenderer('d M Y'),
        },{
            dataIndex: "dpd_current", text: "Current DPD",  filter: {type: 'number'}, width: 100, align: 'right'
        },{
            dataIndex: "payable", text: "Payable",  filter: {type: 'number'}, width: 100, align: 'right'
        },{
            dataIndex: "total_repayment", text: "Tot. Repayment",  filter: {type: 'number'}, width: 100, align: 'right'
        },{
            dataIndex: "debt_remaining", text: "Debt Balance",  filter: {type: 'number'}, width: 100, align: 'right'
        }],
        windowFormClass: "component.admin.DataAdSlotForm",

        initComponent: function() {
            var me = this;

            Ext.apply(this,{
                viewConfig: {
                    getRowClass: function(record) {
                        // alert(record.data.date_end+" - "+new Date());

                    }
                },
            });
            this.callParent();
        }
    });
