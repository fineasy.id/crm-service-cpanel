<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;
Ext.define("${componentName}", {
    extend: "Ext.form.Panel",
    bodyStyle: 'background-color:#ffffff;',
    title: "Contract",
    border: false,
    bodyPadding: 10,
    parentWindow: null,
    formData: null,
    initComponent: function() {
        var me = this;
        var formData = me.formData;

        var topBar = Ext.create("component.contract.ContractCardFormToolbar", {
            formData: me.formData,
            parentWindow: me.parentWindow,
            gridStore: me.gridStore,
            mainForm: me
        });

        var fsContract = Ext.create("Ext.form.FieldSet", {
            defaultType: 'textfield',
            title: "Contract Detail",
            defaults: {
                allowBlank: false,
                readOnly: true
            },
            items: [
                Ext.create("component.abstract.GenericFieldContainer",{
                    items:[
                        {
                            fieldLabel: 'Contract ID#',  name: "contract_id", value: (formData)?formData.contract_id:"", readOnly: true
                        },{
                            fieldLabel: 'Status',  name: "status_name", value: (formData)?formData.status_name:"", readOnly: true
                        },{
                            fieldLabel: 'Created On',  name: "ins_on", value: (formData)?formData.ins_on:"", readOnly: true
                        },{
                            fieldLabel: 'Modified On',  name: "mod_on", value: (formData)?formData.mod_on:"", readOnly: true
                        },{
                            fieldLabel: 'Start date',  name: "contract_start_on", value: (formData)?formData.contract_start_on:"", readOnly: true
                        },{
                            fieldLabel: 'Start deduction',  name: "contract_charged_on", value: (formData)?formData.contract_charged_on:"", readOnly: true
                        },{
                            fieldLabel: 'End date',  name: "contract_end_on", value: (formData)?formData.contract_end_on:"", readOnly: true, colspan: 2
                        }
                    ]
                })
            ]
        });

        var fsOwner = Ext.create("Ext.form.FieldSet", {
            defaultType: 'textfield',
            title: "Customer Detail",
            defaults: {
                allowBlank: false,
                readOnly: true
            },
            items: [
                Ext.create("component.abstract.GenericFieldContainer",{
                    items:[
                        {
                            fieldLabel: 'Easypay ID#',  name: "easypay_id", value: (formData)?formData.easypay_id:"", readOnly: true
                        },{
                            fieldLabel: 'Name',  name: "full_name", value: (formData)?formData.full_name:"", readOnly: true
                        },{
                            fieldLabel: 'Mobile',  name: "mobile", value: (formData)?formData.mobile:"", readOnly: true
                        },{
                            fieldLabel: 'KTP',  name: "reg_id", value: (formData)?formData.reg_id:"", readOnly: true
                        }
                    ]
                })
            ]
        });

        Ext.apply(this,{
            tbar: topBar,
            items: [
                fsContract,
                fsOwner
            ]
        });

        this.callParent();
    }
});
