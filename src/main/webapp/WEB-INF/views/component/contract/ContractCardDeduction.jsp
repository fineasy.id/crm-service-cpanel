<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;
Ext.define("${componentName}", {
    extend: "component.abstract.GenericTabGrid",
    layout:"fit",
    topBarMenuNew: false,
    contractId: null,
    storeFields:[
        { name: 'id',		type: 'number' },
        { name: 'contract_id',		type: 'number' },
        { name: 'seq',		type: 'number' },
        { name: 'deduct_on',	type: 'date', dateFormat: 'Y-m-d' },
        { name: 'amount',		type: 'number' },
        { name: 'prev_balance',		type: 'number' },
        { name: 'repayment',		type: 'number' },
        { name: 'total_repayment',		type: 'number' },
        { name: 'debt_remaining',		type: 'number' },
        { name: 'payable',		type: 'number' },
        { name: 'dpd',		type: 'number' },
        { name: 'ins_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'mod_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' }
    ],
    storeUrl: "${dataProvider}/list.json",
    storeSorters: [{
        property: "seq",
        direction: "ASC"
    }],
    gridColumn: [
        {
            dataIndex: "seq", text: "Deduction #",  filter: {type: 'number'}, width: 100
        },
        {
            dataIndex: "deduct_on", text: "Date", renderer: Ext.util.Format.dateRenderer('d M Y'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
        },
        {
            dataIndex: "prev_balance", text: "Prev. Balance", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "amount", text: "Amount", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "repayment", text: "Repayment", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "payable", text: "Payable", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "total_repayment", text: "Total Repayment", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "debt_remaining", text: "Debt Remaining", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "dpd", text: "DPD", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "remain_amount", text: "Remaining Amount", width: 110, xtype: 'numbercolumn', format:'0,000', align: 'right'
        },
        {
            dataIndex: "mod_on", text: "Modified On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }, hidden: true
        }
    ],
    initComponent: function() {
        var me = this;

        Ext.apply(this,{
            extraParams: {
                filtered: true,
                contractId: me.contractId
            },
            viewConfig: {
                getRowClass: function(record) {
                    // alert(record.data.date_end+" - "+new Date());

                }
            },
        });
        this.callParent();
    }
});