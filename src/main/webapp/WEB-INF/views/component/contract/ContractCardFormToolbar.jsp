<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: 'Ext.toolbar.Toolbar',
    formData: null,
    parentWindow: null,
    initComponent: function() {
        var me = this;
        var formData = me.formData;

        Ext.apply(this,{

        });

        this.callParent();
        if (formData.status_id=="101") {
            me.add('->');
            var buttonActivate = Ext.create('Ext.button.Button', {
                text: 'Activate',
                iconCls: 'accept',
                handler: function() {
                    Ext.Msg.confirm("Contract activation","Are you sure to activate?", function(buttonResult) {
                        if (buttonResult != 'yes') return;
                        Ext.Ajax.request({
                            url: '<c:url value="/data/contract/ContractAction/activate.json" />',
                            method: 'POST',
                            params: {
                                "contractId": formData.contract_id,
                                "${_csrf.parameterName}": "${_csrf.token}"
                            },
                            success: function(response){
                                var obj = Ext.decode(response.responseText);
                                if (obj.success) {
                                    Ext.toast({
                                        bodyStyle: 'background-color:#ffffff;',
                                        title: 'Contract activation on progress!',
                                        html: "Contract activation is on progress, please check later by pressing refresh button!",
                                        iconCls: "tick",
                                        icon: Ext.Msg.ERROR,
                                        align: "t"
                                    });
                                    me.parentWindow.close();
                                } else {
                                    Ext.Msg.alert('Failed!', obj.msg, function(){});
                                }
                            },
                            failure: function(transport){
                                Ext.Msg.alert('Failed!', 'Failed during activation!', function(){});
                            }
                        });

                    });
                }
            });
            me.add(buttonActivate);
        }
    }
});