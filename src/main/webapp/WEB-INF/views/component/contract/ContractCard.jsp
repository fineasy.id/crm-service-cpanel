<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.window.Window",
    layout: 'fit',
    width: 800,
    height: 650,
    deskCollection: false,
    formData: null,
    initComponent: function() {
        var me = this;
        var formData = me.formData;

        var form = Ext.create("component.contract.ContractCardForm",{
            formData: me.formData,
            parentWindow: me,
            gridStore: me.gridStore
        });

        var cashflow = Ext.create("component.cashflow.CashflowList", {
            title: "Cashflow",
            contractId: formData.contract_id,
            extraParams: {
                filtered: true,
                contractId: formData.contract_id
            }
        });

        var deduction = Ext.create("component.contract.ContractCardDeduction", {
            title: "Deduction Schedule",
            contractId: formData.contract_id
        });

        var summary = Ext.create("component.contract.ContractCardSummary", {
            title: "Summary",
            contractId: formData.contract_id
        });

        var collection = Ext.create("component.contract.ContractCardCollectionLog", {
            title: "Desk Collection",
            contractId: formData.contract_id
        });

        Ext.apply(this,{
            modal: true,
            tbar: (me.deskCollection)?Ext.create("component.contract.ContractCardCollectionToolbar",{
                formData:me.formData,
                parentWindow: me
            }):null,
            title: 'Contract Card #'+formData.contract_id,
            items: [
                {
                    cls: 'panel-no-border',
                    border: false,
                    xtype: 'tabpanel', // TabPanel itself has no title
                    activeTab: 0,      // First tab active by default
                    items: [
                        form,
                        summary,
                        deduction,
                        cashflow,
                        collection
                    ]
                }
            ]
        });

        this.callParent();

        if (formData.status_id==101) {

        }
    }
});