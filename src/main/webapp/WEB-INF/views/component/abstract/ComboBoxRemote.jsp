<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

    Ext.define("${componentName}", {
        extend: "Ext.form.field.ComboBox",
        valueField: 'id',
        displayField: 'name',
        queryMode: 'local',
        editable: false,
        typeAhead: true,
        triggerAction: 'all',
        forceSelection:true,
        allowBlank: false,
        autoLoad: null,
        addParams: {},
        url: null,
        extraParams: {},
        onLoadData: function(c, records, successful, eOpts) { },
        initComponent: function() {
            var me = this;
            me.extraParams["${_csrf.parameterName}"] = "${_csrf.token}";

            var store1 = Ext.create('Ext.data.Store', {
                fields: ['id','name'],
                autoLoad: me.autoLoad,

                proxy: {
                    noCache: false,
                    type: 'ajax',
                    url: me.url,
                    actionMethods: {
                        read: 'POST'
                    },
                    extraParams: me.extraParams,
                    reader: {
                        type:'json',
                        root: 'rows'
                    }
                },
                listeners: {
                    load: function(c, records, successful, eOpts) {
                        me.onLoadData(me, records, successful, eOpts);
                    }
                }
            });

            Ext.apply(this, {
                store: store1
            })

            this.callParent();
        }
    });