<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

Ext.define("${componentName}", {
	extend: "Ext.form.FieldContainer",
	columns: 4,
    defaults: {
        // applied to each contained panel
        bodyStyle: 'padding:5px'
    },
	combineErrors: false,
	defaultType: 'textfield',
	allowBlank: false,
	items:[],
	initComponent: function() {
		var me = this;

    	Ext.apply(this, {    		    		
    		defaults: {
    			xtype: 'textfield',
    	        hideLabel: false,
				margin: '5 10 0 0',
    	        allowBlank: me.allowBlank
    	    },
            fieldDefaults: {
                labelAlign: 'top'
            },
            layout: {
                type: 'table',
                columns: me.columns
            },

    	});
    	
    	this.callParent();
	}
});