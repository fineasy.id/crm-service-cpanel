<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

Ext.override(Ext.grid.View, { enableTextSelection: true });
Ext.define("${componentName}", {
	extend: "Ext.grid.Panel",
	layout:"fit",
	plugins: 'gridfilters',
	storeFields:[],
	storeSorters: [{
        property: "mod_on",
        direction: "DESC"
    }],
    storeUrl: "",
    gridStore: null,
    storeId: null,
    gridTopBar: null,
    gridColumn: [],
    topBarMenuNew: true,
    topBarMenuNewAction: null,
    searchBox: true,
    windowFormClass:"",
    extraParams: {},
    windowReportClass: null,
    viewConfig: {
        enableTextSelection: true,
        getRowClass: function(record) {                	
            if (!record.data.is_active) {
            	return 'inactive-row';                   		
            }
        }
    },    
	initComponent: function() {
		var me = this;
		var queryRef = null;

		me.gridTopBar = Ext.create("Ext.toolbar.Toolbar", {
            items: [
                '->',
                {
                    text: "Export ...",
                    iconCls: "printer",
                    handler: function(e) {
                        console.log("exporting query ref: "+queryRef);
                        window.location.href = '<c:url value="/export/csv/" />'+queryRef;
                    }
                }
            ]
        });

        var searchField = Ext.create("Ext.form.field.Text", {

        });

        if (me.searchBox) {
            me.gridTopBar.add("-")
            me.gridTopBar.add("Find")
            me.gridTopBar.add(searchField);
            me.gridTopBar.add({
                text: "Find",
                iconCls: 'folder_explore',
                handler: function(s) {
                    me.gridStore.reload();
                }
            });

        }

		if (me.topBarMenuNew) me.gridTopBar.insert(0,{
            text: "New...",
            iconCls: "add",
            handler: me.topBarMenuNewAction
        });

        me.extraParams["${_csrf.parameterName}"] = "${_csrf.token}";

		/** STORE **/
		me.gridStore = Ext.create("Ext.data.JsonStore",{
            pageSize: 50,
            id: (me.storeId!=null)?me.storeId:'store-'+randInt(99999,999999)+'-'+randInt(1111,11111),
            remoteSort: true,
            autoLoad: false,
            remoteFilter: true,
            fields: me.storeFields, // 
            sorters: me.storeSorters,
            proxy: Ext.create('Ext.data.proxy.Ajax',{
                url: me.storeUrl, // update
                autoLoad: false,
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                extraParams: me.extraParams,
                reader: {
                    keepRawData: true,
                    type: 'json',
                    root: 'rows',
                    totalProperty: 'count'
                },
                simpleSortMode: true
            }),
            listeners: {
                load: function(s,r,success,ops) {
                    var o = s.getProxy().getReader().rawData;
                    queryRef = o.queryRef;
                },
                beforeload: function(s) {
                    s.getProxy().extraParams.searchText = searchField.getValue();
                }
            }
        });
		Ext.apply(this,{		
			store: me.gridStore,
			columns: me.gridColumn,
			listeners: {
                show: function() {
                    //me.gridStore.load({});
                },
                cellclick: function (o, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                    if (e.target.getAttribute('xaction') == 'edit') {
                    	var w = Ext.create(me.windowFormClass, {     
                    		dataRecord: record,
                    		gridStore: me.gridStore
                    	});
                    	w.show();
                    }
                }                
            },
            tbar: me.gridTopBar,
            bbar: Ext.create('Ext.toolbar.Paging', {
                store: me.gridStore,
                displayInfo: true,
                displayMsg: 'Displaying data {0} - {1} of {2}',
                emptyMsg: "No data to display",
                items: [
                    '-',
                    {
                        iconCls: 'page_refresh',
                        text: "Clear Filter",
                        handler: function() {
                            searchField.setValue("");
                            gridFilter = me.filters;
                            gridFilter.clearFilters(false);
                        }
                    }
                ]
            })            
		});
		
		this.callParent();
	}

});