<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

Ext.define("${componentName}", {
	extend: "Ext.grid.Panel",
	plugins: 'gridfilters',
	storeFields:[],
	storeSorters: [{
        property: "mod_time",
        direction: "DESC"
    }],
    storeUrl: "",
    gridStore: null,
    addParams: {},
    disableNew: false,
    formExtraParams: {},
    formParams: {},
    gridColumn: [],
    windowFormClass:"",    
    viewConfig: {
        getRowClass: function(record) {                	
            if (!record.data.is_active) {
            	return 'inactive-row';                   		
            }
        }
    },    
	initComponent: function() {
		var me = this;
		var baseParams = {
                "${_csrf.parameterName}": "${_csrf.token}"
        }
		
		var extraParams = mergeObjects(baseParams, me.addParams);
		
		/** STORE * */
		me.gridStore = Ext.create("Ext.data.JsonStore",{
            pageSize: 50,
            remoteSort: true,
            remoteFilter: true,
            fields: me.storeFields, // 
            sorters: me.storeSorters,
            proxy: Ext.create('Ext.data.proxy.Ajax',{
                url: me.storeUrl, // update
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                extraParams: extraParams,
                reader: {
                    type: 'json',
                    root: 'rows',
                    totalProperty: 'count'
                },
                simpleSortMode: true
            }),
            listeners: {
                load: function(s,r,success,ops) {                    
                }
            }
        });
		Ext.apply(this,{		
			store: me.gridStore,
			columns: me.gridColumn,
			listeners: {
                afterrender: function() {
                    me.gridStore.load({});
                },
                cellclick: function (o, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                    if (e.target.getAttribute('xaction') == 'edit') {
                    	var baseParams = {
                    			dataRecord: record,
                        		gridStore: me.gridStore,
                        		formExtraParams: me.formExtraParams
                        }
                        var formParams = mergeObjects(baseParams, me.formParams);
                    	var w = Ext.create(me.windowFormClass, formParams);
                    	w.show();
                    }
                }                
            },
            tbar: Ext.create("Ext.toolbar.Toolbar", {
            	items: [
            	    {
            	    	text: "New...",
            	    	iconCls: "add",
            	    	disabled: me.disableNew,
                        handler: function() {
                        	var baseParams = {
                        		gridStore: me.gridStore,
                        		formExtraParams: me.formExtraParams
                        	}
                        	var formParams = mergeObjects(baseParams, me.formParams);
                        	var w = Ext.create(me.windowFormClass, formParams);
                        	w.show();
                        }
            	    }
            	]
            }),
            bbar: Ext.create('Ext.toolbar.Paging', {
                store: me.gridStore,
                displayInfo: true,
                displayMsg: 'Displaying data {0} - {1} of {2}',
                emptyMsg: "No data to display",
                items: [
                    '-',
                    {
                        iconCls: 'page_refresh',
                        text: "Clear Filter",
                        handler: function() {
                            gridFilter = me.filters;
                            gridFilter.clearFilters(false);
                        }
                    }
                ]
            })            
		});
		
		this.callParent();
	}

});