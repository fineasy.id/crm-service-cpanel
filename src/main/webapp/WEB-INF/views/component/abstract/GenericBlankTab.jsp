<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

    Ext.define("${componentName}", {
        extend: "Ext.panel.Panel",
        layout:"fit",
        initComponent: function() {
            var me = this;

            var queryRef = null;

            if (me.topBarMenuNew) me.gridTopBar.insert(0,{
                text: "New...",
                iconCls: "add",
                handler: function() {
                    var w = Ext.create(me.windowFormClass, {
                        gridStore: me.gridStore
                    });
                    w.show();
                }
            })

            Ext.apply(this,{
                listeners: {
                    afterrender: function() {
                    }
                }
            });

            this.callParent();
        }
    });