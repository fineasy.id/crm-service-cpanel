<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a=true;
Ext.define("${componentName}", {
    extend: "Ext.window.Window",
    gridStore: null,
    formData:null,
    initComponent: function() {
        var me = this;
        Ext.apply(this, {
            modal: true,
            listeners: {
                afterrender: function() {
                    Ext.getBody().unmask();
                }
            }
        });
        this.callParent();
    }
});