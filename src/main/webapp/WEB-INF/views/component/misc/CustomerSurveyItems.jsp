<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

var __a = true;
Ext.define("${componentName}", {
    extend: "Ext.window.Window",
    layout: 'fit',
    surveyItems: [],
    surveyId: null,
    contactId: null,
    parentRecord: null,
    width: 600,
    height: 350,
    initComponent: function() {
        var me = this;
        var _id = me.surveyId;
        var contactId = me.contactId;


        var updateData = function(action, rec) {

            var params = rec.getData();
            params.surveyId = _id;
            params.action = action;
            params["contactId"] = contactId;
            params["${_csrf.parameterName}"] = "${_csrf.token}";

            Ext.Ajax.request({
                url: '<c:url value="/data/misc/CustomerSurvey/" />'+action+".json",
                method: 'POST',
                params: params,
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                },
                failure: function(response){
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Failed!', obj.msg, function(){
                    });
                }
            });
        }

        var grid1 = Ext.create('Ext.tree.Panel', {
            xtype: 'tree-grid',
            reserveScrollbar: true,
            useArrows: true,
            border: false,
            rootVisible: false,
            multiSelect: false,
            singleExpand: true,
            root: {
                text: '-',
                expanded: true,
                children: me.surveyItems
            },
            columns: [
                {
                    xtype: 'treecolumn', //this is so we know which column will show the tree
                    text: 'Options',
                    dataIndex: 'name',
                    cellWrap: true,
                    flex: 2,
                    sortable: true
                },
                {
                    xtype: 'widgetcolumn',
                    width: 50,
                    dataIndex: 'name',
                    widget: {
                        align: 'center',
                        xtype: 'checkbox',
                        defaultBindProperty: 'enabled',
                        listeners: {
                            afterrender: function(c) {
                                var rec = c.getWidgetRecord();
                                if (rec.get("leaf")!=1) c.setHidden(true);
                            },
                            change: function( c, newValue, oldValue ) {
                                var rec = c.getWidgetRecord();
                                if (newValue) {
                                    if (rec.get("show_text")) {
                                        Ext.Msg.prompt('Please set value', '<div style="margin-bottom:10px;">'+rec.get('name')+'</div>', function(btn, text) {
                                            if (btn == 'ok') {
                                                rec.set("other_option", text);
                                                updateData('insert', rec);
                                            }
                                        });
                                    } else {
                                        updateData('insert', rec);
                                    }
                                } else {
                                    if (rec.get("show_text")) {
                                        rec.set("other_option", "");
                                    }
                                    updateData('delete', rec);
                                }
                            }
                        }
                    }
                },{
                    text: "Custom option",
                    width: 120,
                    dataIndex: 'other_option'
                }

            ]
        });

        Ext.apply(this,{
            modal: true,
            items: [
                grid1
            ],
            tbar: {
                items: [
                    '->',
                    {
                        text: 'OK',
                        iconCls: 'accept',
                        handler: function () {
                            me.parentRecord.set("completed", true);
                            me.close();
                        }
                    }
                ]
            }
        });

        this.callParent();
    }
});