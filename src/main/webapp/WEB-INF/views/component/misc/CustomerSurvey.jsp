<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

var __a = true;

Ext.define("${componentName}", {
    extend: "Ext.window.Window",
    layout: 'fit',
    surveyItems: [],
    surveyId: null,
    contactId: null,
    width: 800,
    height: 550,

    initComponent: function() {
        var me = this;
        var _id = me.surveyId;
        var contactId = me.contactId;

        var grid = Ext.create('Ext.tree.Panel', {
            xtype: 'tree-grid',
            reserveScrollbar: true,
            useArrows: true,
            border: false,
            rootVisible: false,
            multiSelect: false,
            singleExpand: false,
            viewConfig:{
                markDirty:false
            },
            root: {
                text: '-',
                expanded: true,
                children: me.surveyItems
            },
            columns: [
                {
                    xtype: 'treecolumn', //this is so we know which column will show the tree
                    text: 'Task',
                    dataIndex: 'name',
                    flex: 2,
                    sortable: true,
                    cellWrap: true
                },{
                    text: "Completed?",
                    align: 'center',
                    dataIndex: 'completed',
                    renderer: function (value, record) {
                        if (value) {
                            return '<img src="<c:url value="/img/icon/accept.png" />"/>';
                        }
                        return '';
                    }
                }

            ],
            listeners: {
                cellclick:function ( c, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
                    if (cellIndex!=0) return;
                    if (!record.data.leaf) return;
                    if (record.get("completed")) {
                        return;
                    }
                    Ext.Ajax.request({
                        url: '<c:url value="/data/misc/CustomerSurvey/listItem.json" />',
                        method: 'POST',
                        params: {
                            parentId: record.get("item_id"),
                            "${_csrf.parameterName}": "${_csrf.token}"
                        },
                        success: function(response){
                            var obj = Ext.decode(response.responseText);
                            var w = Ext.create("component.misc.CustomerSurveyItems", {
                                contactId: contactId,
                                surveyItems: obj.items,
                                title: "Choose options",
                                surveyId: me.surveyId,
                                parentRecord: record
                            });
                            w.show();
                        },
                        failure: function(response){
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Failed!', obj.msg, function(){
                            });
                        }
                    });
                }
            }
        });

        Ext.apply(this,{
            modal: true,
            title: 'Customer Survey',
            items: [
                grid
            ]
        });

        this.callParent();
    }
});