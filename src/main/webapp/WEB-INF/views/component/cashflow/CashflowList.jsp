<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

    Ext.define("${componentName}", {
        extend: "component.abstract.GenericTabGrid",
        layout:"fit",
        topBarMenuNew: true,
        contractId: null,
        storeFields:[
            { name: 'id',		type: 'number' },
            { name: 'contract_id',		type: 'number' },
            { name: 'category_name',		type: 'string' },
            { name: 'status_name',		type: 'string' },
            { name: 'type_name',		type: 'string' },
            { name: 'amount',		type: 'number' },
            { name: 'trx_date',	type: 'date', dateFormat: 'Y-m-d' },
            { name: 'ins_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' },
            { name: 'mod_on',	type: 'date', dateFormat: 'Y-m-d H:i:s' }
        ],
        storeUrl: "${dataProvider}/list.json",

        windowFormClass: "component.admin.DataAdSlotForm",

        initComponent: function() {
            var me = this;

            Ext.apply(this,{
                gridColumn: [{
                    xtype:'actioncolumn',
                    width:50,
                    align: 'center',
                    items: [{

                    },{
                        iconCls: 'edit',
                        tooltip: 'Edit',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            ajaxRequest({
                                url: "<c:url value="/data/cashflow/CashflowList/getDetail.json" />",
                                params: {
                                    "id": rec.data.id
                                },
                                onSuccess: function(o) {
                                    var w = Ext.create("component.cashflow.CashflowForm",{
                                        title: "Cashflow #"+rec.data.id,
                                        formData: o.data,
                                        gridStore: me.gridStore
                                    });
                                    w.show();
                                }
                            });
                        }
                    }]
                },{
                    dataIndex: "id", text: "Transaction ID", filter: { type: 'number'}, width: 90, align: 'right'
                },{
                    dataIndex: "trx_date", text: "Date", renderer: Ext.util.Format.dateRenderer('d M Y'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
                },{
                    dataIndex: "type_name", text: "Type",  filter: {type: 'string'}, width: 100
                },{
                    dataIndex: "contract_id", text: "Contract ID", filter: { type: 'number'}, width: 90, align: 'right'
                },{
                    dataIndex: "ins_on", text: "Created On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
                },{
                    dataIndex: "mod_on", text: "Modified On", renderer: Ext.util.Format.dateRenderer('d M Y H:i'), width: 110, filter: { type: 'date', dateFormat: 'Y-m-d' }
                },{
                    dataIndex: "status_name", text: "Status",  filter: {type: 'string'}
                },{
                    dataIndex: "category_name", text: "Category",  filter: {type: 'string'}, width: 100
                },{
                    dataIndex: "amount", text: "Amount",  filter: {type: 'number'}, width: 100, xtype: 'numbercolumn', format:'0,000', align: 'right'
                }],
                viewConfig: {
                    getRowClass: function(record) {
                        // alert(record.data.date_end+" - "+new Date());

                    }
                },
                topBarMenuNewAction: function() {
                    var w = Ext.create("component.cashflow.CashflowForm",{
                        title: "New cashflow",
                        contractId: me.contractId,
                        gridStore: me.gridStore
                    });
                    w.show();
                }
            });
            this.callParent();
        }
    });
