<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
var __a=true;

Ext.define("${componentName}", {
    extend: "component.abstract.GenericWindow",
    gridStore: null,
    contractId: null,
    formData:null,
    initComponent: function() {
        var me = this;
        var formData = me.formData;
        var contractId = me.contractId;

        var fieldContractId = Ext.create("Ext.form.field.Text",{
            fieldLabel: 'Contract #',
            name: 'contractId',
            readOnly: (formData||contractId)?true:false,
            maskRe: /[0-9]/,
            value: (formData)?formData.contract_id:((contractId)?contractId:""),
        });

        var fieldTrxDate = Ext.create("Ext.form.field.Date",{
            xtype: 'datefield',
            anchor: '100%',
            format: 'Y-m-d',
            name: "trxDate",
            fieldLabel: 'Transaction date',
            value: (formData)?formData.trx_date:new Date(),
            maxValue: new Date()
        });

        var fieldType = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Flow type',
            autoLoad: true,
            name: "typeId",
            value: (formData)?formData.type_id:"",
            url: "<c:url value="/data/misc/DataComboBox/cashflowType.json" />"
        });

        var fieldCategory = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Category',
            autoLoad: true,
            name: "categoryId",
            value: (formData)?formData.category_id:"",
            url: "<c:url value="/data/misc/DataComboBox/cashflowCategory.json" />"
        });

        var fieldStatus = Ext.create("component.abstract.ComboBoxRemote",{
            fieldLabel: 'Status',
            autoLoad: true,
            name: "statusId",
            value: (formData)?formData.status_id:"",
            url: "<c:url value="/data/misc/DataComboBox/cashflowStatus.json" />"
        });
        var fieldAmount = Ext.create("Ext.form.field.Text",{
            fieldLabel: 'Amount',
            name: 'amount',
            maskRe: /[0-9]/,
            value: (formData)?formData.amount:"",
        });

        var fs1 = Ext.create("Ext.form.FieldSet",{
            title: "Cashflow Detail",
            items: [
                Ext.create("component.abstract.GenericFieldContainer",{
                    columns: 3,
                    items:[
                        fieldContractId,
                        fieldTrxDate,
                        fieldType,
                        fieldCategory,
                        fieldStatus,
                        fieldAmount,
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Remark',
                            name: 'remark',
                            value: (formData)?formData.remark:"",
                            colspan:2,
                            allowBlank: true,
                            width: 300
                        }
                    ]
                })
            ]
        });

        var form = Ext.create("Ext.form.Panel",{
            bodyStyle: 'background-color:#ffffff;',
            border: false,
            bodyPadding: 10,
            items: [
                fs1
            ]
        });

        Ext.apply(this,{
            modal: true,
            items: [
                form
            ],
            tbar: {
                items:[
                    '->',
                    {
                        text: "Save",
                        iconCls: 'disk',
                        handler: function() {
                            if (!form.isValid()) return;
                            Ext.Msg.confirm("Save","Are you sure to save?", function(buttonId) {
                                if (buttonId != 'yes') return;
                                var baseForm = form.getForm();
                                baseForm.submit({
                                    waitMsg: 'Updating, please wait ...',
                                    url: "<c:url value="/data/cashflow/CashflowList/update.json" />",
                                    params: {
                                        "${_csrf.parameterName}": "${_csrf.token}",
                                        "id": (formData)?formData.id:null
                                    },
                                    success: function(f,a,r) {
                                        showToast({
                                            title: 'Success!',
                                            text: "Cashflow updated successfully",
                                        });
                                        if (me.gridStore) me.gridStore.reload();
                                        me.close();
                                    },
                                    failure: function(f,a,r) {
                                        handleSubmitFailure(f,a);
                                    }
                                })
                            });
                        }
                    }
                ]
            }
        });

        this.callParent();
    }
});