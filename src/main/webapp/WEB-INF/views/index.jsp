<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page session="true"%>
<c:set var="now" value="<%=new java.util.Date()%>"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
    <head>
        <title>Fineasy &rsaquo; Customer Management System</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <link href="<c:url value="/css/custom.css" />" rel="stylesheet" type="text/css"/>
        <script>
            currentDate = new Date(<fmt:formatDate pattern="yyyy,(MM-1),dd" value="${now}"/>);
            contextPath = "${context}";
            extTheme = 'triton';
        </script>
    </head>
    <body>
    <div class="all-center" id="loading-div">
        <img src="<c:url value="/img/loading-2.gif" />"/>
        <div style="font-family: Arial; font-size: 10pt;">Loading please wait ...</div>

        <script src="<c:url value="/js/jquery-3.3.1.min.js" />"></script>
        <script src="<c:url value="/component/core/StartUp.js" />" type="text/javascript"></script>
    </div>
    </body>
</html>