/**
 * Created by Indra on 14/04/2015.
 */
renderRowLink = function(v,p,r) {
    var html = "<a xaction='edit' href='javascript:void(0);'>"+v+"</a>";
    return html;
}

function randInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

openMainTab = function(module) {
	console.log("Module: "+module.menu_id);
	console.log("module.name_view: "+module.name_view)
    var tabId = "main-tab-"+module.menu_id;
    var tab = Ext.getCmp(tabId);
    var mainTab = Ext.getCmp('maintab-panel')
    if (tab == null) {
        tab = Ext.create(module.name_view, {
        	title: module.text,
        	iconCls: module.iconCls,
        	closable: true,
        	border: false,
            id: tabId
        });
        mainTab.add(tab);
    }
    mainTab.setActiveTab(tab);
}

handleSubmitFailure = function(f,a) {
	switch (a.failureType) {
    case Ext.form.action.Action.CLIENT_INVALID:
    	Ext.Msg.alert({
        	icon: Ext.Msg.ERROR,
        	title: "Failure when submitting",
        	message: "Form fields may not be submitted with invalid values"
        });        
        break;
    case Ext.form.action.Action.CONNECT_FAILURE:
        Ext.Msg.alert({
        	icon: Ext.Msg.ERROR,
        	title: "Network Problem",
        	message: "Network problem while update data!<br/>Please check your internet connection or contact your system administrator!"
        });
        break;
    case Ext.form.action.Action.SERVER_INVALID:
    	Ext.Msg.alert({
        	icon: Ext.Msg.ERROR,
        	title: "Failure when submitting data",
        	message: (a.result.msg)?a.result.msg:"Unknown error!"
        });
	}
}

nl2br = function (str, is_xhtml) {	
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    var newStr = (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    console.log(newStr);
    return newStr;
}