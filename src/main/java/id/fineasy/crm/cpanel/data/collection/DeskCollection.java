package id.fineasy.crm.cpanel.data.collection;

import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static id.fineasy.crm.cpanel.component.DataUtils.COLLECTION_CALL_RESULT_FAILED;

public class DeskCollection extends DataAbstract  {

    public DeskCollection(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject list() throws DataProviderException {
        String condition = "";
        if (request.getParameter("contractId")!=null) condition+= " AND contract_id="+request.getParameter("contractId");
        return simpleList("_collection_call_log", condition);
    }

    public JSONObject getQueue() throws Exception {
        final JSONObject jsonObject = new JSONObject();
        Integer deskCollDpd = Integer.parseInt(configUtils.getConfig("collection.desk.queue.dpd"));

        MapSqlParameterSource sqlParams = dataUtils.newSqlParams();
        sqlParams.addValue("dpdCurrent", deskCollDpd);
        sqlParams.addValue("userId", userDetail.getId());

        TransactionTemplate trxTemplate = new TransactionTemplate(trxManager);
        trxTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
        trxTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    String sql = " SELECT c.*, r.contract_id as reserved_contract_id FROM _contract c " +
                            " LEFT JOIN collection_call_reserve r ON (c.contract_id=r.contract_id)   " +
                            " WHERE dpd_current>=:dpdCurrent " +
                            " AND c.contract_id NOT IN (SELECT contract_id FROM collection_call_log WHERE contract_id=c.contract_id AND callback_on>=NOW())" +
                            " AND c.contract_id NOT IN (SELECT contract_id FROM collection_call_reserve WHERE contract_id=c.contract_id AND ins_by != :userId) " +
                            " ORDER BY reserved_contract_id DESC, c.dpd_current LIMIT 0,1 ";

                    List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, sqlParams);
                    if (rows.size()<=0) {
                        jsonObject.put("success", false).put("msg", "No contract to be collected!");
                    } else {
                        jsonObject.put("success", true).put("data", rows.get(0));
                        Long contractId = jsonObject.getJSONObject("data").getLong("contract_id");
                        sqlParams.addValue("contractId", contractId);
                        sql = " INSERT IGNORE INTO collection_call_reserve (contract_id, ins_by) VALUES (:contractId,:userId) ";
                        jdbcTemplate.update(sql, sqlParams);
                    }
                } catch (Exception e) {
                    jsonObject.put("success", false).put("msg", "Error: "+e);
                    log.error("Error: "+e,e);
                    transactionStatus.setRollbackOnly();
                }
            }
        });

        return jsonObject;
    }

    public JSONObject updateCall() throws Exception {
        final JSONObject jsonObject = new JSONObject();
        Long contractId = Long.parseLong(request.getParameter("contractId"));
        Integer resultId = null;
        Long successResult = Long.parseLong(request.getParameter("successResult"));

        MapSqlParameterSource sqlParams = dataUtils.newSqlParams();
        sqlParams.addValue("contractId", contractId);
        sqlParams.addValue("remark", request.getParameter("note"));

        sqlParams.addValue("userId", userDetail.getId());

        Calendar cal = Calendar.getInstance();
        if (successResult == 1) {
            cal.add(Calendar.HOUR, 24);
            resultId = Integer.parseInt(request.getParameter("resultId"));
        } else {
            cal.add(Calendar.MINUTE, 5);
            resultId = COLLECTION_CALL_RESULT_FAILED;
        }
        sqlParams.addValue("resultId", resultId);
        sqlParams.addValue("callbackOn", cal.getTime());

        TransactionTemplate trxTemplate = new TransactionTemplate(trxManager);
        trxTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
        trxTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    String sql = " INSERT INTO collection_call_log (ins_by,mod_by,contract_id,result_id,remark,callback_on) " +
                            " VALUES (:userId,:userId,:contractId,:resultId,:remark, :callbackOn) ";
                    jdbcTemplate.update(sql, sqlParams);
                    sql = " DELETE FROM collection_call_reserve WHERE contract_id=:contractId ";
                    jdbcTemplate.update(sql, sqlParams);
                } catch (Exception e) {
                    log.error("Error: "+e,e);
                    transactionStatus.setRollbackOnly();
                    jsonObject.put("success", false).put("msg", "Error: "+e);
                }
            }
        });

        jsonObject.put("success", true);
        return jsonObject;
    }
}
