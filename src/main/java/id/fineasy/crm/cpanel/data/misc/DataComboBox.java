package id.fineasy.crm.cpanel.data.misc;

import id.fineasy.crm.cpanel.component.UserDetail;
import id.fineasy.crm.cpanel.data.DataAbstract;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.object.MappingSqlQuery;

import javax.servlet.http.HttpServletRequest;

public class DataComboBox extends DataAbstract {
    public DataComboBox(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
    }

    public JSONObject amountList() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("productId",request.getParameter("productId"));
        params.addValue("seq",request.getParameter("seq"));

        String sql = " SELECT DISTINCT amount as id, FORMAT(amount,0) as name FROM product_matrix_mca WHERE product_id=:productId AND sequence=getValidLoanSeq(:productId,:seq) ORDER BY id ";
        return simpleList(sql, params);
    }

    public JSONObject termList() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("productId",request.getParameter("productId"));
        params.addValue("seq",request.getParameter("seq"));

        String sql = " SELECT DISTINCT tenor as id, FORMAT(tenor,0) as name FROM product_matrix_mca WHERE product_id=:productId AND sequence=getValidLoanSeq(:productId,:seq) ORDER BY id ";
        return simpleList(sql, params);
    }

    public JSONObject applicationStatus() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tag", request.getParameter("statusTag"));

        String sql = " SELECT id,name FROM application_status WHERE tag=:tag ";
        return simpleList(sql, params);
    }

    public JSONObject callbackTimeList() {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = " SELECT id,name FROM application_callback_time WHERE 1 ORDER BY id";
        return simpleList(sql, params);

    }
    public JSONObject educationList() {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = " SELECT id,name FROM contact_edu WHERE 1 ORDER BY id";
        return simpleList(sql, params);

    }

    public JSONObject maritalList() {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = " SELECT id,name FROM contact_marital_status WHERE 1 ORDER BY id";
        return simpleList(sql, params);

    }

    public JSONObject childrenList() {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = " SELECT id,name FROM contact_children WHERE 1 ORDER BY id";
        return simpleList(sql, params);

    }

    public JSONObject businessYearsList() {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = " SELECT id,name FROM contact_business_years WHERE 1 ORDER BY id";
        return simpleList(sql, params);

    }

    public JSONObject loanPurposeList() {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = " SELECT id,name FROM application_loan_purpose WHERE 1 ORDER BY id";
        return simpleList(sql, params);
    }

    public JSONObject collectionCallResult() {
        MapSqlParameterSource params = dataUtils.newSqlParams();
        params.addValue("successResult", Integer.parseInt(request.getParameter("successResult")));
        String sql = " SELECT id,name FROM collection_call_result WHERE call_success=:successResult ";
        return simpleList(sql, params);
    }

    public JSONObject cashflowType() {
        MapSqlParameterSource params = dataUtils.newSqlParams();
        String sql = " SELECT id,name FROM cashflow_type WHERE 1 ORDER BY id";
        return simpleList(sql, params);
    }

    public JSONObject cashflowCategory() {
        MapSqlParameterSource params = dataUtils.newSqlParams();
        String sql = " SELECT id,name FROM cashflow_category WHERE 1 ORDER BY id";
        return simpleList(sql, params);
    }

    public JSONObject cashflowStatus() {
        MapSqlParameterSource params = dataUtils.newSqlParams();
        String sql = " SELECT id,name FROM cashflow_status WHERE 1 ORDER BY id";
        return simpleList(sql, params);
    }


}
