package id.fineasy.crm.cpanel.data.cashflow;

import id.fineasy.crm.cpanel.component.UserDetail;
import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.Map;

import static id.fineasy.crm.cpanel.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT;

public class CashflowList extends DataAbstract {
    public CashflowList(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject list() throws DataProviderException {
        String condition = "";
        if (request.getParameter("contractId")!=null) condition+= " AND contract_id="+request.getParameter("contractId");
        return simpleList("_cashflow", condition);
    }

    public JSONObject getDetail() throws Exception {
        JSONObject jsonResult = new JSONObject();

        Long id = Long.parseLong(request.getParameter("id"));
        String sql = " SELECT * FROM _cashflow WHERE id=:id ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        Map<String,Object> data = jdbcTemplate.queryForMap(sql, params);
        jsonResult.put("success", true);
        jsonResult.put("data", data);


        return jsonResult;
    }

    public JSONObject update() throws Exception {
        if (!userDetail.has(UserDetail.ACL_CASHFLOW_UPDATE)) return UserDetail.forbiddenReturn();

        JSONObject jsonResult = new JSONObject();

        MapSqlParameterSource params = new MapSqlParameterSource();

        Long contractId = Long.parseLong(request.getParameter("contractId"));
        params.addValue("contractId", contractId);

        String sql = "";

        /**
        String sql = " SELECT count(*) FROM _contract WHERE contract_id=:contractId AND is_active=1 ";
        Long found = jdbcTemplate.queryForObject(sql, params, Long.class);
        if (found <=0) {
            jsonResult.put("success", false);
            jsonResult.put("msg", "Contract #"+contractId+" is not valid!");
            return jsonResult;
        }
         **/

        params.addValue("trxDate", request.getParameter("trxDate"));
        params.addValue("typeId", request.getParameter("typeId"));
        params.addValue("categoryId", request.getParameter("categoryId"));
        params.addValue("statusId", request.getParameter("statusId"));
        params.addValue("amount", request.getParameter("amount"));
        params.addValue("remark", request.getParameter("remark"));
        params.addValue("userId", userDetail.getId());

        //params.addValue("contractId",)

        Long id;
        if (request.getParameter("id")==null || request.getParameter("id").isEmpty()) {
            KeyHolder holder = new GeneratedKeyHolder();
            sql = " INSERT INTO cashflow (ins_by, mod_by,contract_id,trx_date,type_id,category_id,status_id,amount,remark) " +
                    " VALUES (:userId,:userId,:contractId,:trxDate,:typeId,:categoryId,:statusId,:amount,:remark) ";
            jdbcTemplate.update(sql, params, holder);
            id = holder.getKey().longValue();
        } else {
            id = Long.parseLong(request.getParameter("id"));
            params.addValue("id", id);
            sql = " UPDATE cashflow SET mod_by=:userId,trx_date=:trxDate,type_id=:typeId,category_id=:categoryId,amount=:amount,status_id=:statusId,remark=:remark  " +
                    " WHERE id=:id ";
            jdbcTemplate.update(sql, params);
        }

        jsonResult.put("success", true);

        messageSender.send(QUEUE_RECALCULATE_CONTRACT, new JSONObject().put("contractId", contractId));

        return jsonResult;
    }
}