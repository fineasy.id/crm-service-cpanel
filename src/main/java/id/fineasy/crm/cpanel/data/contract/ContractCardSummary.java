package id.fineasy.crm.cpanel.data.contract;

import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;

public class ContractCardSummary extends DataAbstract {
    public ContractCardSummary(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject list() throws DataProviderException {
        String condition = "";
        if (request.getParameter("contractId")!=null) condition+= " AND contract_id="+request.getParameter("contractId");
        return simpleList("_contract_summary", condition);
    }
}

