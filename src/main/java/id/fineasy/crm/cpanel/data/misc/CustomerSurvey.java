package id.fineasy.crm.cpanel.data.misc;

import id.fineasy.crm.cpanel.data.DataAbstract;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.lang.Nullable;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CustomerSurvey extends DataAbstract {
    public CustomerSurvey(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject list() {
        JSONObject jsonResult = new JSONObject();
        Long contactId = Long.parseLong(request.getParameter("contactId"));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contactId", contactId);
        String sql = " SELECT count(*) FROM survey_result WHERE contact_id=:contactId ";
        Long found = jdbcTemplate.queryForObject(sql, params, Long.class);
        log.debug("Customer survery, contact #"+contactId+" surveyed before? result: "+found);
        if (found > 0) {
            jsonResult.put("success", false);
        } else {
            jsonResult.put("success", true);
            jsonResult.put("items", generateTree(0L, 0L));
            jsonResult.put("id", UUID.randomUUID().toString());
        }
        return jsonResult;
    }

    public JSONObject listItem() {
        Long parentId = Long.parseLong(request.getParameter("parentId"));
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);
        jsonResult.put("items", generateOptions(parentId));
        return jsonResult;
    }

    private JSONArray generateTree(Long parentId, long isOption) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("parentId", parentId);
        params.addValue("isOption", isOption);
        String sql = " SELECT id as item_id,name,parent_id,is_option,show_text, IF(((SELECT COUNT(*) FROM survey_task WHERE parent_id=t.id AND is_option=:isOption)>0),FALSE,TRUE) as leaf FROM survey_task t WHERE parent_id=:parentId AND is_option=:isOption ORDER BY display_order,id ";
        List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, params);
        for (Map<String,Object> row : rows) {
            if (Integer.parseInt(""+row.get("leaf"))==0) {
                row.put("children", generateTree((long) row.get("item_id"), isOption));
                //if (level <= 0)
                row.put("expanded", true);
            }
        }
        return new JSONArray(rows);
    }

    private JSONArray generateOptions(Long parentId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("parentId", parentId);
        String sql = " SELECT id as item_id,name,parent_id,is_option,show_text, IF(((SELECT COUNT(*) FROM survey_task WHERE parent_id=t.id)>0),FALSE,TRUE) as leaf FROM survey_task t WHERE id=:parentId ORDER BY display_order,id ";
        List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, params);
        for (Map<String,Object> row : rows) {
            if (Integer.parseInt(""+row.get("leaf"))==0) {
                row.put("children", generateTree((long) row.get("item_id"), 1));
                row.put("expanded", true);
            }
        }
        return new JSONArray(rows);
    }


    public JSONObject insert() {
        JSONObject jsonResult = new JSONObject();
        MapSqlParameterSource params = new MapSqlParameterSource();
        String refId = request.getParameter("surveyId");
        params.addValue("refId", refId);
        params.addValue("contactId", request.getParameter("contactId"));
        params.addValue("userId", userDetail.getId());

        TransactionTemplate trxTemplate = new TransactionTemplate(trxManager);
        trxTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    String sql = " SELECT id FROM survey_result WHERE ref_id=:refId ";
                    List<Long> ids = jdbcTemplate.queryForList(sql, params, Long.class);
                    Long surveyId;
                    if (ids.size() <= 0) {
                        KeyHolder holder = new GeneratedKeyHolder();
                        sql = " INSERT INTO survey_result (contact_id,ref_id,ins_by,mod_by) VALUES (:contactId,:refId,:userId,:userId) ";
                        jdbcTemplate.update(sql, params, holder);
                        surveyId = holder.getKey().longValue();
                    } else {
                        surveyId = ids.get(0);
                    }
                    log.info("Survey ID: "+surveyId);
                    params.addValue("surveyId", surveyId);
                    params.addValue("taskId", request.getParameter("parent_id"));
                    params.addValue("chooseId", request.getParameter("item_id"));
                    params.addValue("otherText", request.getParameter("other_option"));

                    sql = " INSERT INTO survey_result_items (survey_id,task_id,task_text,choose_id,choose_text,other_text,ins_by) VALUES " +
                            "  (:surveyId,:taskId,(SELECT name FROM survey_task WHERE id=:taskId),:chooseId,(SELECT name FROM survey_task WHERE id=:chooseId),:otherText,:userId) ";
                    jdbcTemplate.update(sql, params);

                    sql = " UPDATE survey_result SET mod_on=NOW() WHERE ref_id=:refId ";
                    jdbcTemplate.update(sql, params);
                } catch (Exception e) {
                    transactionStatus.setRollbackOnly();
                    log.error("Error while inserting survey: "+e);
                }
            }
        });

        return jsonResult;
    }


    public JSONObject delete() {
        JSONObject jsonResult = new JSONObject();

        MapSqlParameterSource params = new MapSqlParameterSource();
        String refId = request.getParameter("surveyId");
        params.addValue("refId", refId);

        String sql = " SELECT id FROM survey_result WHERE ref_id=:refId ";
        List<Long> ids = jdbcTemplate.queryForList(sql, params, Long.class);
        Long surveyId;
        if (ids.size() <= 0) {
            return jsonResult;
        } else {
            surveyId = ids.get(0);
        }

        log.debug("Survey id for deletion: "+surveyId);

        params.addValue("surveyId", surveyId);
        params.addValue("chooseId", request.getParameter("item_id"));
        sql = " DELETE FROM survey_result_items WHERE survey_id=:surveyId AND choose_id=:chooseId ";
        jdbcTemplate.update(sql, params);

        sql = " UPDATE survey_result SET mod_on=NOW() WHERE id=:surveyId ";
        jdbcTemplate.update(sql, params);

        return jsonResult;
    }


}
