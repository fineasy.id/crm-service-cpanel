package id.fineasy.crm.cpanel.data.application;

import id.fineasy.crm.cpanel.component.UserDetail;
import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import id.fineasy.crm.cpanel.utils.DbUtils;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import sun.util.resources.cldr.aa.CalendarData_aa_ER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.*;

public class ApplicationList extends DataAbstract {

    public ApplicationList(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject list() throws DataProviderException {
        return simpleList("_application");
    }

    public JSONObject detail() throws DataProviderException {
        JSONObject jsonObject = new JSONObject();

        try {
            MapSqlParameterSource params = dataUtils.newSqlParams();
            params.addValue("appId", request.getParameter("appId"));

            String sql = " SELECT * FROM _application WHERE app_id=:appId ";
            Map<String,Object> o = dataUtils.getJT().queryForMap(sql, params);
            jsonObject.put("data", o);
            jsonObject.put("success", true);
        } catch (Exception e) {
            jsonObject.put("success", false);
            jsonObject.put("msg", e.getMessage());
        }


        return jsonObject;
    }


}
