package id.fineasy.crm.cpanel.data.application;

import id.fineasy.crm.cpanel.component.HttpUtils;
import id.fineasy.crm.cpanel.component.UserDetail;
import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import id.fineasy.crm.cpanel.utils.DbUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.*;

import static id.fineasy.crm.cpanel.config.ActiveMQConfig.QUEUE_NEW_CONTRACT;

public class PhoneVerification extends DataAbstract {
    public PhoneVerification(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject getUnverifiedApplication() throws DataProviderException {
        final JSONObject jsonResult = new JSONObject();

        jsonResult.put("success", true);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userDetail.getId());

        Boolean isRepeat = Boolean.parseBoolean(request.getParameter("isRepeat"));

        DataSourceTransactionManager trx1 = (DataSourceTransactionManager) context.getBean("trxManager");
        TransactionTemplate trxTemp = new TransactionTemplate(trx1);
        trxTemp.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
        trxTemp.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    String sql = " SELECT * FROM application_verification_reserve r JOIN _application a ON (a.app_id=r.app_id) WHERE r.ins_by=:userId ";
                    List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, params);
                    if (rows.size() > 0) {
                        Map<String,Object> row = rows.get(0);
                        log.debug("There is reserved application for user "+userDetail.getUser().getUsername()+", app ID#: "+row.get("app_id"));
                        jsonResult.put("row", row);
                        //jsonResult.put("msg", "You have an unfinished application, please make a decision first!");
                    } else {
                        sql = " SELECT a.*,MAX(l.ins_on) as last_verification FROM _application a LEFT JOIN  application_verification_log l ON (a.app_id=l.app_id) " +
                                "  WHERE on_process=1 AND (a.callback_on IS NULL OR a.callback_on<=NOW()) ";
                        sql+= (isRepeat)?" AND a.loan_seq>1 ":" AND a.loan_seq <= 1 ";
                        sql+= " AND a.app_id NOT IN (SELECT app_id FROM application_verification_reserve) " +
                                " GROUP BY a.app_id ";
                        sql+= " ORDER BY last_verification,a.mod_on LIMIT 0,1 ";
                        rows = jdbcTemplate.queryForList(sql, params);
                        if (rows.size()<1) {
                            // no app to be verified
                            log.debug("No application to be verified!");
                            jsonResult.put("success", false);
                            jsonResult.put("msg", "No more application to be verified!");
                        } else {
                            Map<String,Object> row = rows.get(0);
                            log.debug("Found new application to be verified, app ID#"+row.get("app_id"));
                            params.addValue("appId", row.get("app_id"));
                            sql = " INSERT INTO application_verification_reserve (app_id, ins_by) VALUES (:appId, :userId) ";
                            jdbcTemplate.update(sql, params);

                            params.addValue("statusId", DbUtils.STATUS_APPLICATION_ON_VERIFICATION);
                            sql = " UPDATE application SET status_id=:statusId,mod_by=:userId WHERE app_id=:appId ";
                            jdbcTemplate.update(sql, params);

                            jsonResult.put("row", row);
                        }
                    }

                } catch (Exception e) {
                    transactionStatus.setRollbackOnly();
                    log.error("error :"+e,e);
                    jsonResult.put("success", false);
                    jsonResult.put("msg", e.getMessage());
                }
            }
        });

        return jsonResult;
    }

    public JSONObject checkAmountIsValid() throws DataProviderException {
        final JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("productId", request.getParameter("productId"));
        params.addValue("loanSeq", request.getParameter("loanSeq"));
        params.addValue("turnover", Long.parseLong(request.getParameter("turnover")));
        params.addValue("unit", 100000);
        params.addValue("amount", request.getParameter("amount"));

        String sql = " SELECT IFNULL(MAX(amount),0) as max_allowed, IFNULL(MAX(amount),0) >= :amount as is_valid FROM `product_loan_limit` WHERE 1\n" +
                " AND product_id=:productId AND loan_seq=:loanSeq AND turnover <= ROUND(:turnover/:unit)*:unit ";

        Map<String,Object> result = jdbcTemplate.queryForMap(sql, params);
        jsonResult.put("success", result.get("is_valid"));
        jsonResult.put("max_allowed", result.get("max_allowed"));

        params.addValue("maxAllowed", result.get("max_allowed"));

        sql = " SELECT IFNULL(MAX(amount),0) FROM  product_matrix_mca WHERE product_id=:productId AND sequence=:loanSeq AND amount <=ROUND(:maxAllowed/:unit)*:unit ";
        Long loanAmount = jdbcTemplate.queryForObject(sql, params, Long.class);
        jsonResult.put("amount", loanAmount);
        params.addValue("loanAmount", loanAmount);

        sql = " SELECT charge FROM product_matrix_mca WHERE product_id=:productId AND sequence=:loanSeq AND amount ";

        return jsonResult;
    }

    public JSONObject getDeductionAmount() throws DataProviderException {
        final JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("productId", request.getParameter("productId"));
        params.addValue("loanSeq", request.getParameter("loanSeq"));
        params.addValue("tenor", Long.parseLong(request.getParameter("tenor")));
        params.addValue("amount", request.getParameter("amount"));

        String sql = " SELECT IFNULL(MAX(charge),0) FROM product_matrix_mca WHERE product_id=:productId AND sequence=:loanSeq AND amount=:amount AND tenor=:tenor ";
        Long charge = jdbcTemplate.queryForObject(sql, params, Long.class);
        if (charge == 0) jsonResult.put("success", false);

        jsonResult.put("charge", charge);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(request.getParameter("tenor")));

        Date endOn = cal.getTime();
        jsonResult.put("end_on", (new SimpleDateFormat("yyyy-MM-dd")).format(endOn));

        return jsonResult;
    }

    public JSONObject pending() {
        if (!userDetail.has(UserDetail.ACL_APPLICATION_UPDATE)) return UserDetail.forbiddenReturn();

        final JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        TransactionTemplate trxTemp = new TransactionTemplate(trxManager);
        trxTemp.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    MapSqlParameterSource params = new MapSqlParameterSource();
                    params.addValue("appId", request.getParameter("app_id"));
                    params.addValue("statusId", request.getParameter("status_id"));
                    params.addValue("note", request.getParameter("note"));
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.MINUTE, Integer.parseInt(request.getParameter("callback_minute")));
                    String callbackTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime()));
                    log.debug("Set callback time: "+callbackTime);
                    params.addValue("callbackTime", callbackTime);
                    params.addValue("userId", userDetail.getId());

                    String sql = " INSERT INTO application_verification_log (app_id,ins_by,status_id,callback_on,note) VALUES (:appId,:userId,:statusId,:callbackTime,:note) ";
                    jdbcTemplate.update(sql, params);

                    sql = " UPDATE application SET status_id=:statusId,mod_by=:userId,callback_on=:callbackTime WHERE app_id=:appId ";
                    jdbcTemplate.update(sql, params);

                    sql = " DELETE FROM application_verification_reserve WHERE app_id=:appId ";
                    jdbcTemplate.update(sql, params);
                    updateContactLastCall(params);
                } catch (Exception e){
                    transactionStatus.setRollbackOnly();
                    log.error("error :"+e,e);
                    jsonResult.put("success", false);
                    jsonResult.put("msg", e.getMessage());
                }
            }
        });
        return jsonResult;
    }

    private void updateContactLastCall(MapSqlParameterSource params) {
        String sql = " UPDATE contact SET last_call=NOW() WHERE contact_id = (SELECT contact_id FROM application WHERE app_id=:appId) ";
        dataUtils.getJT().update(sql, params);
    }

    public JSONObject reject() {
        if (!userDetail.has(UserDetail.ACL_APPLICATION_UPDATE)) return UserDetail.forbiddenReturn();

        final JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("appId", request.getParameter("app_id"));
        params.addValue("statusId", request.getParameter("status_id"));
        params.addValue("note", request.getParameter("note"));
        params.addValue("userId", userDetail.getId());

        TransactionTemplate trxTemp = new TransactionTemplate(trxManager);
        trxTemp.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                boolean allowSms = false;
                Integer holdDays =0;
                String mobile = "";
                try {
                    String sql = " INSERT INTO application_verification_log (app_id,ins_by,status_id,note) VALUES (:appId,:userId,:statusId,:note) ";
                    jdbcTemplate.update(sql, params);

                    sql = " UPDATE application SET status_id=:statusId,mod_by=:userId,callback_on=NULL WHERE app_id=:appId ";
                    jdbcTemplate.update(sql, params);

                    sql = " DELETE FROM application_verification_reserve WHERE app_id=:appId ";
                    jdbcTemplate.update(sql, params);

                    sql = " SELECT * FROM _application WHERE app_id=:appId ";
                    Map<String,Object> appData = jdbcTemplate.queryForMap(sql, params);
                    mobile = ""+appData.get("mobile");
                    String easypayId = ""+appData.get("easypay_id");
                    params.addValue("easypayId", easypayId);

                    Map<String,String> strParams = new HashMap<>();
                    strParams.put("easypay_id", ""+appData.get("easypay_id"));

                    sql = " SELECT send_sms, hold_days FROM application_status WHERE id=:statusId ";
                    Map<String,Object> queryResult = jdbcTemplate.queryForMap(sql, params);

                    allowSms = (boolean) queryResult.get("send_sms");
                    holdDays = Integer.parseInt(""+queryResult.get("hold_days") );
                    params.addValue("holdDays", holdDays);
                    log.debug("Hold days: "+holdDays+", send SMS: "+allowSms);

                    updateContactLastCall(params);
                } catch (Exception e){
                    transactionStatus.setRollbackOnly();
                    log.error("error :"+e,e);
                    jsonResult.put("success", false);
                    jsonResult.put("msg", e.getMessage());
                } finally {
                    if (allowSms) try {
                        log.info("Allowed to send SMS");
                        smsUtils.send("REJECTION", mobile);
                    } catch (Exception e2) { log.error ("Error sending sms: "+e2); }

                    if (holdDays>0) try {
                        String sql = " INSERT INTO contact_telesales_hold (easypay_id,hold_until,ins_by,mod_by) " +
                                " VALUES (:easypayId,ADDDATE(NOW(), INTERVAL :holdDays DAY),:userId,:userId) " +
                                " ON DUPLICATE KEY UPDATE mod_by=:userId, mod_on=NOW(), hold_until=ADDDATE(NOW(), INTERVAL :holdDays DAY) ";
                        jdbcTemplate.update(sql, params);
                    } catch (Exception e2) { log.error ("Error sending sms: "+e2); }
                }
            }
        });

        return jsonResult;
    }

    public JSONObject approve() {
        if (!userDetail.has(UserDetail.ACL_APPLICATION_UPDATE)) return UserDetail.forbiddenReturn();

        Boolean approvalIsSucess;
        final JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        List<String> contactFieldNames = new ArrayList<>();
        List<String> applicationFieldNames = new ArrayList<>();

        MapSqlParameterSource contactParams = new MapSqlParameterSource();
        MapSqlParameterSource applicationParams = new MapSqlParameterSource();

        final Long appId = Long.parseLong(request.getParameter("app_id"));

        contactParams.addValue("contact_id", request.getParameter("contact_id"));
        applicationParams.addValue("app_id", appId);
        applicationParams.addValue("appId", appId);



        applicationParams.addValue("contract_start_on", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        applicationParams.addValue("status_id", DbUtils.STATUS_APPLICATION_APPROVED);
        applicationParams.addValue("user_id", userDetail.getId());
        applicationParams.addValue("callback_on", null);
        applicationFieldNames.add("contract_start_on=:contract_start_on");
        applicationFieldNames.add("status_id=:status_id");
        applicationFieldNames.add("mod_by=:user_id");
        applicationFieldNames.add("callback_on=:callback_on");

        Enumeration<String> rawFieldNames = request.getParameterNames();
        while(rawFieldNames.hasMoreElements()) {
            String rawFieldName = rawFieldNames.nextElement();
            if (rawFieldName.startsWith("#")) {
                // handle contact
                String fieldName = rawFieldName.replace("#","");
                contactFieldNames.add(fieldName+"=:"+fieldName);
                contactParams.addValue(fieldName, request.getParameter(rawFieldName));
            }
            else if (rawFieldName.startsWith("!")) {
                String fieldName = rawFieldName.replace("!","");
                applicationFieldNames.add(fieldName+"=:"+fieldName);
                applicationParams.addValue(fieldName, request.getParameter(rawFieldName));
            }
        }

        String sqlContact = " UPDATE contact SET "+ StringUtils.join(contactFieldNames,",")+" WHERE contact_id=:contact_id ";
        String sqlApplication = " UPDATE application SET "+ StringUtils.join(applicationFieldNames,",")+" WHERE app_id=:app_id ";

        TransactionTemplate trxTemp = new TransactionTemplate(trxManager);
        trxTemp.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
        Boolean trxIsSuccess = trxTemp.execute(new TransactionCallback<Boolean>() {
            @Override
            public Boolean doInTransaction(TransactionStatus transactionStatus) {
                Boolean success = false;
                try {
                    jdbcTemplate.update(sqlContact, contactParams);
                    jdbcTemplate.update(sqlApplication, applicationParams);

                    String sql = " INSERT INTO application_verification_log (app_id,ins_by,status_id) VALUES (:app_id,:user_id,:status_id) ";
                    jdbcTemplate.update(sql, applicationParams);

                    sql = " DELETE FROM application_verification_reserve WHERE app_id=:app_id ";
                    jdbcTemplate.update(sql, applicationParams);

                    sql = " SELECT * FROM _application WHERE app_id=:app_id ";
                    Map<String,Object> appData = jdbcTemplate.queryForMap(sql, applicationParams);
                    String mobile = ""+appData.get("mobile");
                    String easypayId = ""+appData.get("easypay_id");
                    Map<String,String> strParams = new HashMap<>();
                    strParams.put("easypay_id", ""+appData.get("easypay_id"));
                    success = true;
                    updateContactLastCall(applicationParams);

                } catch (Exception e){
                    transactionStatus.setRollbackOnly();
                    log.error("error :"+e,e);
                    jsonResult.put("success", false);
                    jsonResult.put("msg", e.getMessage());
                    success = false;
                }
                return success;
            }
        });

        if (trxIsSuccess) {
            log.info("Transaction is success!");
            messageSender.send(QUEUE_NEW_CONTRACT, "appId", appId);;
        }


        return jsonResult;
    }

    public JSONObject updateContact() throws DataProviderException {
        if (!userDetail.has(UserDetail.ACL_APPLICATION_UPDATE)) return UserDetail.forbiddenReturn();
        
        JSONObject jsonResult = new JSONObject();

        String fieldName = request.getParameter("fieldName");
        String fieldValue = request.getParameter("fieldValue");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(fieldName, fieldValue);
        params.addValue("contactId", request.getParameter("contactId"));

        String sql = " UPDATE contact SET `"+fieldName+"`=:"+fieldName+" WHERE contact_id=:contactId ";
        jdbcTemplate.update(sql, params);

        jsonResult.put("success", true);

        return jsonResult;
    }

    public JSONObject updateApplication() throws DataProviderException {
        if (!userDetail.has(UserDetail.ACL_APPLICATION_UPDATE)) return UserDetail.forbiddenReturn();

        JSONObject jsonResult = new JSONObject();

        String fieldName = request.getParameter("fieldName");
        String fieldValue = request.getParameter("fieldValue");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(fieldName, fieldValue);
        params.addValue("appId", request.getParameter("appId"));

        String sql = " UPDATE application SET `"+fieldName+"`=:"+fieldName+" WHERE app_id=:appId ";
        jdbcTemplate.update(sql, params);

        jsonResult.put("success", true);

        return jsonResult;
    }
}
