package id.fineasy.crm.cpanel.data;

import id.fineasy.crm.cpanel.component.*;
import id.fineasy.crm.cpanel.utils.DbUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.security.core.userdetails.User;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class DataAbstract {
    protected Logger log;
    protected final ApplicationContext context;
    protected final HttpServletRequest request;
    protected final NamedParameterJdbcTemplate jdbcTemplate;
    protected final UserDetail userDetail;
    protected final DataSourceTransactionManager trxManager;
    protected final DataUtils dataUtils;
    protected final ConfigUtils configUtils;
    protected final SmsUtils smsUtils;
    protected final MessageSender messageSender;



    public DataAbstract(ApplicationContext context, HttpServletRequest request) {
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
        this.context = context;
        this.request = request;
        this.jdbcTemplate = (NamedParameterJdbcTemplate) context.getBean("namedJdbcTemplate");
        this.userDetail = (UserDetail) context.getBean("userDetail");
        this.userDetail.init();
        this.trxManager = (DataSourceTransactionManager) context.getBean("trxManager");;
        this.dataUtils = (DataUtils) context.getBean("dataUtils");
        this.configUtils = (ConfigUtils) context.getBean("configUtils");
        this.smsUtils = (SmsUtils) context.getBean("smsUtils");
        this.messageSender = (MessageSender) context.getBean("messageSender");
    }

    public JSONObject simpleList(String tableName) throws DataProviderException {
        return simpleList(tableName, "");
    }

    public JSONObject simpleList(String tableName, String staticCondition) throws DataProviderException {
        JSONObject jsonResult = new JSONObject();

        String sqlTemplate = "SELECT ${fields} FROM `"+tableName+"` WHERE 1 "+staticCondition;
        List<String> fieldNames = new ArrayList<>();
        fieldNames.add("*");

        jsonResult = DbUtils.query(jdbcTemplate, sqlTemplate, fieldNames, null, request);

        return jsonResult;
    }

    public MapSqlParameterSource createSqlParam() {
        return new MapSqlParameterSource();
    }

    public JSONObject simpleList(String sql, MapSqlParameterSource params) {
        JSONObject jsonResult = new JSONObject();

        List<Map<String,Object>> qr = jdbcTemplate.queryForList(sql, params);

        jsonResult.put("success", true);
        jsonResult.put("rows", DbUtils.queryToJson(qr));
        return jsonResult;
    }
}
