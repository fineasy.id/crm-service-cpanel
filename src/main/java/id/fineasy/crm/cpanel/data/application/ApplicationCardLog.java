package id.fineasy.crm.cpanel.data.application;

import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import id.fineasy.crm.cpanel.utils.DbUtils;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

public class ApplicationCardLog extends DataAbstract {

    public ApplicationCardLog(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject list() throws DataProviderException {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("app_id", request.getParameter("app_id"));
        String sqlTemplate = "SELECT ${fields} FROM `_application_verification_log` WHERE 1 AND app_id=:app_id";
        List<String> fieldNames = new ArrayList<>();
        fieldNames.add("*");
        fieldNames.add("ins_on as mod_on");

        JSONObject jsonResult = DbUtils.query(jdbcTemplate, sqlTemplate, fieldNames, params, request);
        return jsonResult;
    }
}
