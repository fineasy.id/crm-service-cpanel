package id.fineasy.crm.cpanel.data.application;

import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

import static id.fineasy.crm.cpanel.component.DataUtils.CHANNEL_TELESALES_NEW_APPLICATION;
import static id.fineasy.crm.cpanel.component.DataUtils.CHANNEL_TELESALES_REPEAT;

public class Telesales extends DataAbstract {

    public static final int QUEUE_TYPE_INCOMPLETE_APPLICATION = 1;
    public static final int QUEUE_TYPE_ELIGIBLE_REPEAT = 2;
    public static final int QUEUE_TYPE_EXTERNAL_SOURCE = 3;

    public Telesales(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject newApplication() throws Exception {
        final JSONObject jsonResult = new JSONObject();

        if (hasReservedApplication()) {
            jsonResult.put("success", true);
            jsonResult.put("hasUnprocessed", true);
            jsonResult.put("msg","You have 1 unfinished application, please continue before create a new one!");
            return jsonResult;
        }

        TransactionTemplate trxTemp = new TransactionTemplate(trxManager);
        trxTemp.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
        trxTemp.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    String easypayId = null;
                    Boolean isRepeat = Boolean.parseBoolean(request.getParameter("isRepeat"));
                    String searchId = request.getParameter("searchId").trim();
                    if (!searchId.trim().equals("")) {
                        String searchMobile = searchId.replaceFirst("^0", "+62");
                        String searchEasypayId = searchId.trim();
                        easypayId = dataUtils.findEasyPayId(searchEasypayId, searchMobile, isRepeat);
                    } else {
                        easypayId = dataUtils.getAvailableEasypayAccount(isRepeat);
                    }

                    log.info("Found easypay ID #"+easypayId);

                    JSONObject contactDetail = dataUtils.getContactByEasypayId(easypayId);
                    /**
                    if (contactDetail == null) {
                        // create new contact based on easypay data!
                        contactDetail = dataUtils.createNewContactBasedOnEasypayId(easypayId);
                    }
                     **/
                    Long contactId = contactDetail.getLong("contact_id");
                    Long appId = dataUtils.duplicationCheck(contactId);

                    // create new application ...
                    if (appId == null)
                        appId = createNewApplication(contactDetail);

                    reserveApplication(appId);

                    // for testing
                    //transactionStatus.setRollbackOnly();
                    jsonResult.put("success", true);
                } catch (Exception e) {
                    log.error("Error: "+e,e);
                    transactionStatus.setRollbackOnly();
                    jsonResult.put("success", false);
                    jsonResult.put("msg", ""+e);
                }
            }
        });

        if (!jsonResult.getBoolean("success")) throw new Exception(jsonResult.getString("msg"));

        return jsonResult;
    }

    private boolean hasReservedApplication() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userDetail.getId());

        String sql = " SELECT count(*) FROM application_verification_reserve WHERE ins_by=:userId ";
        int found = jdbcTemplate.queryForObject(sql, params, Integer.class);
        if (found > 0) return true;
        else return false;
    }


    private Long createNewApplication(JSONObject contactDetail)  {
        Long contactId = contactDetail.getLong("contact_id");

        MapSqlParameterSource params = new MapSqlParameterSource();
        Integer loanSeq = dataUtils.getLoanSeq(contactId);
        Integer channelId = CHANNEL_TELESALES_NEW_APPLICATION;

        if (loanSeq > 1) {
            log.info("Customer #"+contactId+" has eligible repeat!");
            channelId = CHANNEL_TELESALES_REPEAT;
        }

        params.addValue("userId", userDetail.getId());
        params.addValue("contactId", contactId);
        params.addValue("productId", configUtils.getConfig("default.product.id"));
        params.addValue("loanSeq", loanSeq);
        params.addValue("statusId", dataUtils.STATUS_APPLICATION_ON_VERIFICATION);
        params.addValue("channelId", channelId);

        String sql = " INSERT INTO application (ins_by,mod_by,contact_id,product_id,loan_seq,status_id,channel_id) " +
                " VALUES (:userId,:userId,:contactId,:productId,:loanSeq,:statusId,:channelId) ";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, params, holder);
        Long appId = holder.getKey().longValue();
        params.addValue("appId", appId);

        log.info("Created application #"+appId);
        return appId;
    }

    private void reserveApplication(Long appId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userDetail.getId());
        params.addValue("appId", appId);

        String sql = " DELETE FROM application_verification_reserve WHERE ins_by=:userId OR app_id=:appId ";
        jdbcTemplate.update(sql, params);

        sql = " INSERT INTO application_verification_reserve (app_id,ins_by) VALUES (:appId,:userId) ";
        jdbcTemplate.update(sql, params);

    }
}
