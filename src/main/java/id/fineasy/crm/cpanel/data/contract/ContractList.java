package id.fineasy.crm.cpanel.data.contract;

import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.Map;

public class ContractList extends DataAbstract {
    public ContractList(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject list() throws DataProviderException {
        return simpleList("_contract");
    }

    public JSONObject detail() throws DataProviderException {
        JSONObject jsonObject = new JSONObject();

        try {
            MapSqlParameterSource params = dataUtils.newSqlParams();
            params.addValue("contractId", request.getParameter("contractId"));

            String sql = " SELECT * FROM _contract WHERE contract_id=:contractId ";
            Map<String,Object> o = dataUtils.getJT().queryForMap(sql, params);
            jsonObject.put("data", o);
            jsonObject.put("success", true);
        } catch (Exception e) {
            jsonObject.put("success", false);
            jsonObject.put("msg", e.getMessage());
        }


        return jsonObject;
    }


}
