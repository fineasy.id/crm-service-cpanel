package id.fineasy.crm.cpanel.data;

public class DataProviderException extends Exception {
    public DataProviderException(String errorMessage) {
        super(errorMessage);
    }
}
