package id.fineasy.crm.cpanel.data.contract;

import id.fineasy.crm.cpanel.component.HttpUtils;
import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

import static id.fineasy.crm.cpanel.config.ActiveMQConfig.QUEUE_ACTIVATE_CONTRACT;

public class ContractAction extends DataAbstract {
    public ContractAction(ApplicationContext context, HttpServletRequest request) {
        super(context, request);
        log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    }

    public JSONObject activate() throws DataProviderException, Exception {
        JSONObject jsonObject = new JSONObject();
        String url = configUtils.getConfig("api.contract.url")+"/activate";
        Map<String,Object> getParams = new HashMap<>();
        getParams.put("contract_id", request.getParameter("contractId"));
        getParams.put("activate_by", userDetail.getId());

        JSONObject jsonReq = new JSONObject();
        jsonReq.put("contractId", request.getParameter("contractId"));
        jsonReq.put("activateBy", userDetail.getId());

        messageSender.send(QUEUE_ACTIVATE_CONTRACT, jsonReq);
        jsonObject.put("success", true);
        return jsonObject;
    }
}
