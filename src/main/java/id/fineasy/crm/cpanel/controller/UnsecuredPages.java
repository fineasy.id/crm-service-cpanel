package id.fineasy.crm.cpanel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;

@Controller
public class UnsecuredPages {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping("/login")
    public String login(){

        return "login";
    }

    @RequestMapping("/logout")
    public void logout(HttpServletRequest httpRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpRequest.logout();
        httpServletResponse.sendRedirect(httpRequest.getContextPath()+"/login");
    }
}
