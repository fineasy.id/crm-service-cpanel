package id.fineasy.crm.cpanel.controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.Map;

@RestController
public class UserTool {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private PasswordEncoder passwordEncoder;

    String respText = "";

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value = {"/user/add"}, produces = "text/plain;charset=UTF-8")
    public ResponseEntity<String> deduction(HttpServletRequest request) {

        respText = "";

        NamedParameterJdbcTemplate jdbcTemplate = (NamedParameterJdbcTemplate) context.getBean("namedJdbcTemplate");

        try {
            String fullName = request.getParameter("name").trim().toUpperCase();
            String[] names  = fullName.split(" ");
            String firstName = names[0];
            String lastName = names[names.length-1];
            String userName = firstName.toLowerCase()+"."+lastName.toLowerCase();
            String password = request.getParameter("password").trim();

            final MapSqlParameterSource params = new MapSqlParameterSource();

            DataSourceTransactionManager trxManager = (DataSourceTransactionManager) context.getBean("trxManager");
            TransactionTemplate trxTemplate = new TransactionTemplate(trxManager);
            trxTemplate.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    try {

                        params.addValue("fullName", fullName);
                        params.addValue("firstName", firstName);
                        params.addValue("lastName", lastName);
                        params.addValue("userName", userName);
                        params.addValue("password", password);

                        String passwordBcrypt = passwordEncoder.encode(password);
                        params.addValue("passwordBcrypt", passwordBcrypt);

                        String sql = " INSERT INTO contact (first_name,last_name,full_name,type_id) VALUES (:firstName,:lastName,:fullName,2) ";
                        KeyHolder holder = new GeneratedKeyHolder();
                        jdbcTemplate.update(sql, params, holder);

                        Long contactId = holder.getKey().longValue();
                        params.addValue("contactId", contactId);

                        sql = " INSERT INTO crm_user (contact_id,username,password) VALUES (:contactId,:userName,:passwordBcrypt) ";
                        jdbcTemplate.update(sql, params);

                        String[] groups = request.getParameter("groups").trim().split(",");
                        for (String groupId : groups) {
                            params.addValue("groupId", groupId);
                            sql = " INSERT INTO crm_user_group_map (contact_id, group_id) VALUES (:contactId,:groupId) ";
                            jdbcTemplate.update(sql, params);
                        }

                        respText+= contactId+";";
                        respText+= firstName+";";
                        respText+= lastName+";";
                        respText+= fullName+";";
                        respText+= userName+";";
                        respText+= password+";";

                        //transactionStatus.setRollbackOnly();
                    } catch (Exception e) {
                        transactionStatus.setRollbackOnly();
                        if (e instanceof org.springframework.dao.DuplicateKeyException) {
                            processIfDulplicate(request, params);
                        }
                        respText = "Error: "+e;

                    }
                }
            });
        } catch (Exception e) {
            respText = "Error: "+e;
        }

        return defaultReturn(respText);
    }

    private void processIfDulplicate(HttpServletRequest request, MapSqlParameterSource params) {
        NamedParameterJdbcTemplate jdbcTemplate = (NamedParameterJdbcTemplate) context.getBean("namedJdbcTemplate");

        try {
            String sql = " SELECT * FROM crm_user WHERE username=:userName ";
            Map<String,Object> row = jdbcTemplate.queryForMap(sql, params);
            params.addValue("contactId", row.get("contact_id"));



            String[] groups = request.getParameter("groups").trim().split(",");
            for (String groupId : groups) {
                params.addValue("groupId", groupId);
                sql = " INSERT IGNORE INTO crm_user_group_map (contact_id, group_id) VALUES (:contactId,:groupId) ";
                jdbcTemplate.update(sql, params);
            }
        } catch (Exception e) {
            log.error("Error: "+e,e);
        }
    }

    private ResponseEntity defaultReturn(String respText) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.TEXT_PLAIN);
        return new ResponseEntity<String>(respText, headers, httpStatus);
    }
}
