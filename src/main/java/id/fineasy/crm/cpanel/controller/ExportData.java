package id.fineasy.crm.cpanel.controller;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@Secured({"ROLE_ADMIN"})
public class ExportData {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    private ServletContext context;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value = {"/export/{formatName}/{queryRef}"}, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> export(HttpServletRequest request, HttpServletResponse response, @PathVariable("formatName") String formatName, @PathVariable("queryRef") String queryRef) {
        log.debug("Exporting data to: "+formatName);
        log.debug("Query ref: "+queryRef);
        HttpHeaders headers = new HttpHeaders();

        response.setContentType("application/force-download");

        HttpStatus httpStatus = HttpStatus.OK;

        String contentData = "Invalid data";

        String sql =  getSqlRef(queryRef);
        log.debug("Get SQL: "+queryRef+", query: "+sql);

        try {
            File f= exportCsv(sql);
            contentData = FileUtils.readFileToString(f, "UTF-8");
        } catch (Exception e) {
            log.error("Error export CSV: "+e,e);
        }

        response.setContentLength((int)contentData.length());
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition","attachment; filename=\"exported.csv\"");

        return new ResponseEntity<String>(contentData, headers, httpStatus);
    }

    private String getSqlRef(String ref) {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("refId", ref);

            String sql = " SELECT query FROM crm_query_ref WHERE ref_id=:refId ";
            return namedJdbcTemplate.queryForObject(sql, params, String.class);
        } catch (Exception e) {
            log.warn("Query with ref: "+ref+" invalid! Error: "+e);
            return null;
        }
    }

    private File exportCsv(String sql) throws IOException {
        sql+= " LIMIT 0, 65500";;
        MapSqlParameterSource params = new MapSqlParameterSource();
        List<Map<String,Object>> list = namedJdbcTemplate.queryForList(sql, params);
        String[] HEADERS = null;
        if (list.size()>0) {
            Map<String,Object> r = list.get(0);
            ArrayList<String> fieldNames = new ArrayList<>(r.keySet());

            HEADERS = new String[fieldNames.size()];
            HEADERS = fieldNames.toArray(HEADERS);
        }

        File outFile = File.createTempFile("crm-","csv");

        FileWriter out = new FileWriter(outFile);
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.EXCEL.withHeader(HEADERS))) {
            for (Map<String,Object> r : list) {
                for(String header: HEADERS) {
                    printer.print(r.get(header));
                }
                printer.println();
            }
        }
        return outFile;
    }
}
