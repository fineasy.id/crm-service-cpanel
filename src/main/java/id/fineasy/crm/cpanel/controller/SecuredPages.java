package id.fineasy.crm.cpanel.controller;

import id.fineasy.crm.cpanel.component.UserDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Controller
@Secured({"ROLE_ADMIN"})
public class SecuredPages {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    private ServletContext context;

    @Autowired
    private UserDetail userDetail;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loginMessage(){
        //int ct = namedJdbcTemplate.queryForObject("SELECT COUNT(*) FROM crm_user_group ", new HashMap<>(), Integer.class);
        return "index";
    }

    @RequestMapping(value = "/component/{folderName}/{scriptName}.js", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
    public String getComponent(ModelMap model, HttpServletRequest request, HttpServletResponse response, @PathVariable("folderName") String folderName, @PathVariable("scriptName") String scriptName) {
        Map<String,String> attributes = new HashMap<String, String>();

        userDetail.init();

        try {
            Class<?> c =  Class.forName("id.fineasy.crm.cpanel.component."+folderName+"."+scriptName);
            Method initMethod = c.getMethod("init", ServletContext.class, HttpServletRequest.class, NamedParameterJdbcTemplate.class, UserDetail.class);
            Method processMethod = c.getMethod("process");
            Method getAttributesMethod = c.getMethod("getAttributes");
            Object obj = c.newInstance();
            initMethod.invoke(obj, request.getServletContext(), request, namedJdbcTemplate, userDetail);
            processMethod.invoke(obj);
            attributes = (Map<String, String>) getAttributesMethod.invoke(obj);

        } catch (ClassNotFoundException e) {
            //log.error("Class not found: "+e);
        } catch (Exception e) {
            log.error("Error component invoke: "+e, e);
        }

        updateLastActivityTime();

        attributes.put("componentName", "component."+folderName+"."+scriptName);
        attributes.put("dataProvider", "data/"+folderName+"/"+scriptName);
        model.addAllAttributes(attributes);

        String viewName = "component/"+folderName+"/"+scriptName;
        return viewName;
    }

    private void updateLastActivityTime() {
        String sql = " UPDATE crm_user SET last_active=NOW() WHERE username=:userName ";
        MapSqlParameterSource paramMap = new MapSqlParameterSource();
        paramMap.addValue("userName", userDetail.getUser().getUsername());
        namedJdbcTemplate.update(sql, paramMap);

    }
}
