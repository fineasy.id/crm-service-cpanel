package id.fineasy.crm.cpanel.controller;

import id.fineasy.crm.cpanel.component.UserDetail;
import id.fineasy.crm.cpanel.data.DataAbstract;
import id.fineasy.crm.cpanel.data.DataProviderException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.el.MethodNotFoundException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Controller
@Secured({"ROLE_ADMIN"})
public class DataProvider {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private UserDetail userDetail;

    @Autowired
    DataSourceTransactionManager trxManager;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value = {"/data/{folderName}/{className}/{methodName}.json"}, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getComponent(HttpServletRequest request, @PathVariable("folderName") String folderName, @PathVariable("className") String className, @PathVariable("methodName") String methodName) {
        userDetail.init();
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            Class[] classArgs = new Class[]{ ApplicationContext.class, HttpServletRequest.class };
            String dataClassName = "id.fineasy.crm.cpanel.data."+folderName+"."+className;
            log.debug("Class name: "+dataClassName);

            Class<?> c =  Class.forName(dataClassName);

            Constructor constructor = c.getConstructor(classArgs);
            DataAbstract obj = (DataAbstract) constructor.newInstance(context, request);
            Method processMethod = c.getMethod(methodName);
            JSONObject rawResult = (JSONObject) processMethod.invoke(obj);
            jsonResult = rawResult;
        }
        catch (InvocationTargetException e) {
            Throwable t = e.getCause();
            httpStatus = HttpStatus.BAD_REQUEST;
            jsonResult.put("success", false);
            jsonResult.put("msg", t.getMessage());
            log.error("Error on calling method: "+t,t);
        }
        catch (Exception e) {
            log.error("Class/method unknwon error: "+e,e);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            jsonResult.put("success", false);
            jsonResult.put("msg", "Unknwon error!");
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }
}
