package id.fineasy.crm.cpanel.config;

import id.fineasy.crm.cpanel.component.CustomAuthenticationSuccessHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import java.lang.invoke.MethodHandles;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {

        log.info("Bcrypt 123: "+ passwordEncoder.encode("123"));

        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select username,password,is_active as enabled from crm_user where username=? AND is_active=1")
                .authoritiesByUsernameQuery("select username,'ROLE_ADMIN' as role from crm_user where username=?")
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/xxxx")
                .access("hasRole('ROLE_ADMIN')")
                .and()
                .formLogin()
                .successHandler(customAuthenticationSuccessHandler)
                .loginPage("/login")
                .failureUrl("/login?failed")
                .usernameParameter("username")
                .passwordParameter("password")
                .and()
                .exceptionHandling()
                .accessDeniedPage("/403")
                .and()
                .csrf();
        http.headers().frameOptions().disable();


        /**
        http

                .authorizeRequests()
                .antMatchers("/", "/home").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/ext/**").permitAll()
                .antMatchers("/font-awesome/**").permitAll()

                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/cpanel/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();
         **/
    }

}
