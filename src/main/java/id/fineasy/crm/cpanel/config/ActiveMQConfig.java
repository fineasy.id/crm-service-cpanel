package id.fineasy.crm.cpanel.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@EnableJms
@Configuration
public class ActiveMQConfig {

    public static final String QUEUE_NEW_CONTRACT = "queue-new-contract";
    public static final String QUEUE_ACTIVATE_CONTRACT = "queue-activate-contract";
    public static final String QUEUE_RECALCULATE_CONTRACT = "queue-recalculate-contract";
    public static final String QUEUE_EMAIL = "email-queue";
    public static final String QUEUE_SMS = "queue-sms-sender";


    @Value("${my.activemq.broker-url}") String brokerUrl;
    @Value("${my.activemq.user}") String brokerUsername;
    @Value("${my.activemq.password}") String brokerPassword;

    @Bean
    public ActiveMQConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(brokerUrl);
        connectionFactory.setPassword(brokerUsername);
        connectionFactory.setUserName(brokerPassword);
        return connectionFactory;
    }

    @Bean
    public JmsTemplate jmsTemplate(){
        JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory());
        return jmsTemplate;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setConcurrency("1-1");
        return factory;
    }

}

