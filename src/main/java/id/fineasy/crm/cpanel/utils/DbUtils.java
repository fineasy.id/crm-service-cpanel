package id.fineasy.crm.cpanel.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.namedparam.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DbUtils {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static final int STATUS_REJECTED_MOBILE_EMPTY = 259;
    public static final int STATUS_GATE_UNEXPECTED_ERROR = 991;
    public static final int STATUS_GATE_PASSED_VERIFICATION = 252;
    public static final int STATUS_GATE_PASSED_TELESALES = 253;
    public static final int STATUS_GATE_REJECTED_ALLOWED_MINIMUM_REAPPLY_DAYS = 256;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_APPLICATION = 257;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_AGREEMENT = 258;

    public static final int STATUS_APPLICATION_READY_FOR_VERIFICATION = 301;
    public static final int STATUS_APPLICATION_ON_VERIFICATION = 302;
    public static final int STATUS_APPLICATION_APPROVED = 101;

    public static final int STATUS_CONTRACT_SIGNED = 101;


    public static JSONArray queryToJson2(List<Map<String,Object>> queryResult) {
        JSONArray rows = new org.json.JSONArray();

        for (int i=0;i<queryResult.size();i++) {
            rows.put((Map<String,Object>) queryResult.get(i));
        }

        return rows;
    }

    public static JSONObject query(NamedParameterJdbcTemplate jdbcTemplate, String sqlTemplate, List<String> fieldNames, MapSqlParameterSource params, HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        try {
            String refId = UUID.randomUUID().toString();
            if (params==null) params = new MapSqlParameterSource();

            Map<String,String> subParams = new HashMap<>();

            sqlTemplate+= generateFilter(request);

            subParams.put("fields", "COUNT(*) as ct");
            String sqlCount = StringSubstitutor.replace(sqlTemplate, subParams);
            long rowCount = jdbcTemplate.queryForObject(sqlCount, params, Long.class);
            jsonResult.put("count", rowCount);

            subParams.put("fields", StringUtils.join(fieldNames,","));
            String sql = StringSubstitutor.replace(sqlTemplate, subParams);
            sql+= (request.getParameter("sort")!=null)?" ORDER BY `"+request.getParameter("sort")+"` "+request.getParameter("dir"):"";

            String sqlExport = generateQuery(refId, jdbcTemplate, sql, params);

            sql+= (request.getParameter("start")!=null)?" LIMIT "+request.getParameter("start")+","+request.getParameter("limit"):"";

            log.debug("Generated sql:  "+sql);

            List<Map<String, Object>> queryResult = jdbcTemplate.queryForList(sql,params);
            jsonResult.put("rows", queryToJson(queryResult));
            jsonResult.put("success", true);
            jsonResult.put("queryRef", refId);
        } catch (Exception e) {
            log.error("Error while query: "+e,e);
            jsonResult.put("success", false);
            jsonResult.put("count", 0);
            jsonResult.put("rows", new JSONArray());
        }
        return jsonResult;
    }

    public static String generateQuery(String refId, NamedParameterJdbcTemplate jdbcTemplate, String sqlTemplate, MapSqlParameterSource params) throws SQLException {
        ParsedSql parsedSql = NamedParameterUtils.parseSqlStatement(sqlTemplate);

        Connection conn = jdbcTemplate.getJdbcTemplate().getDataSource().getConnection();

        String finalQuery = new PreparedStatementCreatorFactory(NamedParameterUtils.substituteNamedParameters(parsedSql, params), NamedParameterUtils.buildSqlTypeArray(parsedSql, params))
                .newPreparedStatementCreator(NamedParameterUtils.buildValueArray(parsedSql, params, null))
                .createPreparedStatement(conn).toString();

        finalQuery = finalQuery.substring(finalQuery.indexOf(":") + 1, finalQuery.length());

        conn.close();

        MapSqlParameterSource params2 = new MapSqlParameterSource();
        params2.addValue("query", finalQuery);
        params2.addValue("refId", refId);


        String sql = " INSERT INTO crm_query_ref (ref_id, query) VALUES (:refId,:query) ";
        jdbcTemplate.update(sql, params2);

        return finalQuery;
    }

    public static JSONArray queryToJson(List<Map<String,Object>> queryResult) {
        JSONArray rows = new JSONArray();
        for (int i=0;i<queryResult.size();i++) {
            Map<String,Object> queryRow = queryResult.get(i);
            JSONObject row = new JSONObject();
            Iterator<String> iterator = queryRow.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();

                //if (queryRow.get(key)!=null) log.info("field: "+key+", class: "+queryRow.get(key).getClass());

                if (queryRow.get(key) instanceof java.sql.Timestamp) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    java.sql.Timestamp value = (java.sql.Timestamp) queryRow.get(key);
                    row.put(key, df.format(value));
                } else if (queryRow.get(key) instanceof java.sql.Date) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    java.sql.Date value = (java.sql.Date) queryRow.get(key);
                    row.put(key, df.format(value));
                }
                else if (queryRow.get(key) instanceof java.lang.Boolean) {
                    row.put(key, queryRow.get(key));
                }
                else if (queryRow.get(key)!=null){
                    row.put(key, queryRow.get(key).toString());
                } else {
                    row.put(key, queryRow.get(key));
                }
            }
            rows.put(row);
        }
        return rows;
    }

    public static String generateFilter(HttpServletRequest request) {
        String sqlFilterString = "";
        if (request.getParameter("filter") == null) return "";

        try {
            JSONArray filters = new JSONArray(request.getParameter("filter"));
            for(int i=0;i<filters.length();i++) {
                JSONObject filter = (JSONObject) filters.get(i);
                String operator = (String) filter.get("operator");
                if (filter.get("value") instanceof java.lang.Boolean) {
                    int value = ((Boolean) filter.get("value"))?1:0;
                    sqlFilterString+= " AND "+filter.get("property")+" = "+value+" ";
                }
                else if (operator.equals("in")) {
                    JSONArray values = filter.getJSONArray("value");
                    if (values.length()<=0) continue;
                    ArrayList<String> valueArray = new ArrayList<String>();
                    for (int k=0;k<values.length();k++) {
                        valueArray.add("'"+values.get(k)+"'");
                    }
                    sqlFilterString+= " AND "+filter.get("property")+" IN ("+StringUtils.join(valueArray,",")+") ";
                }
                else if (operator.equals("like")) {
                    sqlFilterString+= " AND "+filter.get("property")+" LIKE '%"+((String) filter.get("value")).replace("'", "\\'")+"%' ";
                } else if (operator.equals("gt")) {
                    sqlFilterString+= " AND "+filter.get("property")+" > '"+filter.get("value")+"' ";
                } else if (operator.equals("lt")) {
                    sqlFilterString+= " AND "+filter.get("property")+" < '"+filter.get("value")+"' ";
                } else if (operator.equals("eq")) {
                    String value = filter.getString("value");
                    if (value.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")) {
                        log.debug("Filter type is date! ");
                        sqlFilterString+= " AND DATE("+filter.get("property")+") = '"+filter.get("value")+"' ";
                    } else
                        sqlFilterString+= " AND "+filter.get("property")+" = '"+filter.get("value")+"' ";
                }
            }
        } catch (Exception e) {
            log.error("Error while parse filter: "+e);
            return "";
        }
        return sqlFilterString;
    }
}
