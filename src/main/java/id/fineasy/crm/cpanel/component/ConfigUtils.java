package id.fineasy.crm.cpanel.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class ConfigUtils {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public String getConfig(String key) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("key", key);
        String sql = " SELECT cfg_val FROM config WHERE cfg_key=:key ";
        try {
            String val = namedJdbcTemplate.queryForObject(sql, params, String.class);
            return val;
        } catch (Exception e) {
            log.error("Error config: "+e);
            return null;
        }
    }
}
