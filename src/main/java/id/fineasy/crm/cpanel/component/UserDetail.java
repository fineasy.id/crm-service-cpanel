package id.fineasy.crm.cpanel.component;

import java.util.Collection;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class UserDetail {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    public static final int ACL_CASHFLOW_UPDATE = 1012;
    public static final int ACL_APPLICATION_UPDATE = 1013;

    private long userId;
    Map<String,Object> userDetail = null;

    User user = null;

    public void init() {
        user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", username);

        String sql = " SELECT * FROM crm_user u JOIN contact c ON (u.contact_id=c.contact_id) WHERE u.username=:username ";
        userDetail = namedJdbcTemplate.queryForMap(sql, params);
        userId = (Long) userDetail.get("contact_id");
    }

    public boolean has(int aclId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userId);
        params.addValue("aclId", aclId);

        String sql = " SELECT COUNT(*) as ct FROM crm_user_group_map m " +
                " JOIN crm_user_group_acl a ON (a.group_id=m.group_id) " +
                " WHERE a.acl_id=:aclId AND m.contact_id=:userId  ";

        long count = namedJdbcTemplate.queryForObject(sql, params, Long.class);

        return (count>0)?true:false;
    }

    public Long getId() {
        return userId;
    }

    public Map<String,Object> getDetail() {
        return userDetail;
    }

    public User getUser() {
        return user;
    }

    public static JSONObject forbiddenReturn() {
        return new JSONObject().put("success", false).put("msg", "Sorry, you are not allowed to modify this record!");
    }
}
