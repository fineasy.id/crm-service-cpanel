package id.fineasy.crm.cpanel.component.core;

import id.fineasy.crm.cpanel.component.ComponentAbstract;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainMenu extends ComponentAbstract {

    @Override
    public void process() {
        JSONArray menus = this.getRootMenus();
        JSONObject menuObject = new JSONObject();
        menuObject.put("items", menus);
        attributes.put("menuItems", menuObject.toString(4));
    }

    private JSONArray getRootMenus() {
        JSONArray rowArray = new JSONArray();
        String sql = " SELECT DISTINCT m.* FROM crm_menu m INNER JOIN crm_user_group_acl a ON (a.acl_id=m.acl_id) ";
        sql+= " INNER JOIN crm_user_group_map g ON (g.group_id=a.group_id) INNER JOIN crm_user u ON (g.contact_id=u.contact_id) ";
        sql+= " WHERE u.username=:userName ";
        sql+= " AND parent_id IS NULL AND enabled=1 ORDER BY menu_order ";
        HashMap<String,Object> params = new HashMap<String,Object>();
        params.put("userName", userDetail.getUser().getUsername().toString());
        List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, params);

        for (int i=0;i<rows.size();i++) {
            Map<String,Object> row = rows.get(i);
            JSONObject rowJson = new JSONObject();
            rowJson.put("xtype", "mainmenupanel");
            rowJson.put("title", row.get("name_menu"));
            JSONArray children = new JSONArray();
            this.getChildMenu(Integer.parseInt(""+row.get("menu_id")), rowJson, 1);
            //rowJson.put("menuItems", children);
            rowArray.put(rowJson);
        }
        return rowArray;
    }

    private void getChildMenu(int parentId, JSONObject parentItems, int level) {
        JSONArray rowArray = new JSONArray();
        String sql = " SELECT DISTINCT menu_id, name_view, action_type, name_menu as text, IF(icon IS NULL,'plugin',icon) as iconCls ";
        sql+= " ,(SELECT count(*) as ct FROM crm_menu m2 WHERE m2.parent_id=m.menu_id) as children_count ";
        sql+= " FROM crm_menu m INNER JOIN crm_user_group_acl a ON (a.acl_id=m.acl_id) ";
        sql+= " INNER JOIN crm_user_group_map g ON (g.group_id=a.group_id) INNER JOIN crm_user u ON (g.contact_id=u.contact_id) ";
        sql+= " WHERE u.username=:userName AND parent_id=:parent_id AND enabled=1 ORDER BY menu_order ";
        HashMap<String,Object> params = new HashMap<String,Object>();
        params.put("parent_id", parentId);
        params.put("userName", userDetail.getUser().getUsername().toString());
        List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, params);

        for (int i=0;i<rows.size();i++) {
            Map<String,Object> row = rows.get(i);

            JSONObject rowJson = new JSONObject(row);

            if (Integer.parseInt(""+row.get("children_count")) > 0) {
                rowJson.put("leaf", false);
                rowJson.put("expanded", true);
                getChildMenu(Integer.parseInt(""+row.get("menu_id")), rowJson, level+1);
            } else {
                rowJson.put("leaf", true);
            }

            rowArray.put(rowJson);
        }
        if (level==1) {
            parentItems.put("menuItems", rowArray);
        } else {
            if (rowArray.length()>0) parentItems.put("children", rowArray);
        }
    }
}
