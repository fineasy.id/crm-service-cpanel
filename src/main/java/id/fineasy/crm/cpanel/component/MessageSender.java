package id.fineasy.crm.cpanel.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class MessageSender {
    @Autowired
    JmsTemplate jmsTemplate;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void send(String destination, JSONObject jsonObject) {
        log.info("Sending message to: "+destination+", message content: "+jsonObject);
        jmsTemplate.convertAndSend(destination, jsonObject.toString());
    }

    public void send(String destination, String keyParam, Object valueParam) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(keyParam, valueParam);
        this.send(destination, jsonObject);
    }
}
