package id.fineasy.crm.cpanel.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Map;

import org.springframework.security.core.userdetails.User;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private User userAuth;

    @Autowired
    public UserDetail userDetail;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication auth) throws IOException, ServletException {
        userAuth = (User) auth.getPrincipal();


        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userName", userAuth.getUsername());

        String sql = " UPDATE crm_user SET last_login=NOW() WHERE username=:userName ";
        namedJdbcTemplate.update(sql, params);

        sql = " SELECT * FROM crm_user WHERE username=:userName ";

        Map<String,Object> userMap = namedJdbcTemplate.queryForMap(sql, params);
        JSONObject userJson = new JSONObject(userMap);

        userDetail.init();

        httpServletRequest.getSession().setAttribute("userJson", userJson);
        httpServletRequest.getSession().setAttribute("userAuth", userAuth);

        log.info("Logged in as: "+userAuth.getUsername()+", detail: "+userJson);

        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/");
    }
}
