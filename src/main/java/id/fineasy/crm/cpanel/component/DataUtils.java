package id.fineasy.crm.cpanel.component;

import id.fineasy.crm.cpanel.data.DataProviderException;
import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

@Component
public class DataUtils {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    DataSourceTransactionManager trxManager;
    @Autowired
    UserDetail userDetail;

    public final static int USER_ID_SYSTEM = 1;
    public static final int STATUS_NEW = 251;
    public static final int STATUS_REJECTED_MOBILE_EMPTY = 259;
    public static final int STATUS_GATE_UNEXPECTED_ERROR = 991;
    public static final int STATUS_GATE_PASSED_VERIFICATION = 252;
    public static final int STATUS_GATE_PASSED_TELESALES = 253;
    public static final int STATUS_GATE_REJECTED_ALLOWED_MINIMUM_REAPPLY_DAYS = 256;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_APPLICATION = 257;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_AGREEMENT = 258;

    public static final int STATUS_APPLICATION_READY_FOR_VERIFICATION = 301;
    public static final int STATUS_APPLICATION_ON_VERIFICATION = 302;
    public static final int STATUS_APPLICATION_APPROVED = 101;

    public static final int TELESALES_SOURCE_TYPE_INCOMPLETE_APPLICATION = 1;
    public static final int TELESALES_SOURCE_TYPE_ELIGIBLE_REPEAT = 2;
    public static final int TELESALES_SOURCE_TYPE_EXTERNAL_DATA = 3;

    public static final int CHANNEL_WEBSITE = 1;
    public static final int CHANNEL_WEBSITE_INCOMPLETE = 2;
    public static final int CHANNEL_TELESALES_NEW_APPLICATION  = 3;
    public static final int CHANNEL_WEBSITE_REPEAT = 4;
    public static final int CHANNEL_TELESALES_REPEAT = 5;

    public static final int COLLECTION_CALL_RESULT_FAILED = 201;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public Map<String,Object> getTmpApplication(long tmpId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        String sql = " SELECT * FROM _application_gate WHERE tmp_id=:tmpId ";
        return namedJdbcTemplate.queryForMap(sql, params);
    }

    public void updateTmpApplicationStatus(long tmpId, int statusId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        params.addValue("statusId", statusId);

        String sql = " UPDATE application_tmp SET status_id=:statusId WHERE tmp_id=:tmpId ";
        namedJdbcTemplate.update(sql, params);
    }

    public void updateTmpApplicationStatusAndProcessed(long tmpId, int statusId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        params.addValue("statusId", statusId);

        String sql = " UPDATE application_tmp SET status_id=:statusId, is_processed=1, processed_on=NOW() WHERE tmp_id=:tmpId ";
        namedJdbcTemplate.update(sql, params);
    }

    public MapSqlParameterSource newSqlParams() {
        return new MapSqlParameterSource();
    }

    public NamedParameterJdbcTemplate getJT() {
        return namedJdbcTemplate;
    }
    public DataSourceTransactionManager getTrx() {
        return trxManager;
    }

    public String findEasyPayId(String easypayId, String mobile, boolean isRepeat) throws DataProviderException {
        log.info("Find easypay ID: "+easypayId+", mobile: "+mobile+", is repeat: "+isRepeat);
        MapSqlParameterSource params = newSqlParams();
        params.addValue("easypayId", easypayId);
        params.addValue("mobile", mobile);

        String sql = " SELECT easypay_id FROM contact m WHERE (easypay_id=:easypayId OR mobile=:mobile) " +
                " AND easypay_id NOT IN (SELECT easypay_id FROM _application_gate WHERE easypay_id=m.easypay_id AND on_process=1) " +
                " AND easypay_id NOT IN (SELECT easypay_id FROM _contract WHERE easypay_id=m.easypay_id AND is_active=1) " +
                " AND m.easypay_id NOT IN (SELECT easypay_id FROM _contact_non_eligible_repeat) ";
        if (isRepeat) {
            sql+= " AND m.easypay_id IN (SELECT easypay_id FROM _contact_eligible_repeat) ";
        } else {
            log.info("Is repeat: "+isRepeat);
            sql+= " AND m.easypay_id NOT IN (SELECT easypay_id FROM _contact_eligible_repeat) ";
        }

        sql+= " LIMIT 0,1 ";

        List<String> ids = namedJdbcTemplate.queryForList(sql, params, String.class);
        if (ids.size()<=0) throw new DataProviderException("Easypay account couldn't be found!");

        return ids.get(0);
    }

    public JSONObject getContactByEasypayId(String easypayId) {
        MapSqlParameterSource params = newSqlParams();
        params.addValue("easypayId", easypayId);
        try {
            String sql = " SELECT * FROM contact WHERE easypay_id=:easypayId ";
            Map<String,Object> row = namedJdbcTemplate.queryForMap(sql, params);
            JSONObject jsonObject = new JSONObject(row);
            return jsonObject;
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject createNewContactBasedOnEasypayId(String easypayId) {
        MapSqlParameterSource params = newSqlParams();
        params.addValue("easypayId", easypayId);

        Map<String,Object> easypayDetail = namedJdbcTemplate.queryForMap("SELECT * FROM easypay_member WHERE easypay_id=:easypayId", params);
        String fullName = easypayDetail.get("name").toString().trim().toUpperCase();
        String[] names = fullName.split(" ");
        String firstName = "";
        String lastName = "";
        String midName = null;
        for (int i=0;i<names.length;i++) {
            if (i==0) firstName = names[0];
            else if (i==1 && names.length>2) midName = names[i];
            else lastName+=names[i]+" ";
        }

        String mobile = easypayDetail.get("mobile").toString().trim().replaceAll("[^0-9\\+]","").replaceFirst("^0","+62");

        params.addValue("firstName", firstName);
        params.addValue("lastName", lastName);
        params.addValue("midName", midName);
        params.addValue("fullName", fullName);
        params.addValue("turnover", easypayDetail.get("turnover"));
        params.addValue("mobile", mobile);

        KeyHolder holder = new GeneratedKeyHolder();
        String sql = " INSERT INTO contact (easypay_id,first_name,mid_name,last_name,full_name,mobile,monthly_turnover) " +
                " VALUES (:easypayId,:firstName,:midName,:lastName,:fullName,:mobile,:turnover) " +
                " ON DUPLICATE KEY UPDATE easypay_id=:easypayId, monthly_turnover=:turnover ";
        namedJdbcTemplate.update(sql, params, holder);
        Long contactId = holder.getKey().longValue();

        log.info("Created contact with ID :"+contactId);
        return getContactByEasypayId(easypayId);
    }

    public Long duplicationCheck(Long contactId) throws Exception {
        MapSqlParameterSource params = newSqlParams();
        params.addValue("contactId", contactId);

        String sql = " SELECT COUNT(*) FROM _contract WHERE contact_id=:contactId AND is_active=1 ";
        if (namedJdbcTemplate.queryForObject(sql, params, Integer.class) > 0) throw new Exception("Customer has active contract!");

        sql = " SELECT * FROM _application WHERE contact_id=:contactId AND on_process=1 LIMIT 1 ";
        List<Map<String,Object>> appList = namedJdbcTemplate.queryForList(sql, params);
        if (appList.size() > 0) {
            JSONObject jsonApp = new JSONObject(appList.get(0));
            params.addValue("appId", jsonApp.get("app_id"));
            params.addValue("userId", userDetail.getId());

            sql = " SELECT COUNT(*) FROM application_verification_reserve WHERE app_id=:appId AND ins_by!=:userId ";
            Integer count = namedJdbcTemplate.queryForObject(sql, params, Integer.class);

            if (count > 0)
                throw new Exception("Customer has on process application by other operator!");

            return jsonApp.getLong("app_id");
        }

        log.info("Duplication check passed!");
        return null;
    }

    public String getAvailableEasypayAccount(boolean isRepeat) throws DataProviderException {
        MapSqlParameterSource params = newSqlParams();
        String sql = " SELECT easypay_id FROM contact m WHERE 1 AND type_id=1 AND mobile IS NOT NULL AND easypay_id IS NOT NULL " +
                " AND easypay_id NOT IN (SELECT easypay_id FROM _application_gate WHERE easypay_id=m.easypay_id AND on_process=1) " +
                " AND easypay_id NOT IN (SELECT easypay_id FROM _application WHERE easypay_id=m.easypay_id AND on_process=1) " +
                " AND easypay_id NOT IN (SELECT easypay_id FROM _contract WHERE easypay_id=m.easypay_id AND is_active=1) " +
                " AND easypay_id NOT IN (SELECT easypay_id FROM contact_telesales_hold WHERE easypay_id=m.easypay_id AND hold_until>NOW()) " +
                " AND easypay_id NOT IN (SELECT easypay_id FROM _contact_non_eligible_repeat) ";
        if (isRepeat) {
            sql+= " AND easypay_id IN (SELECT easypay_id FROM _contact_eligible_repeat) ";
        } else {
            sql+= " AND easypay_id NOT IN (SELECT easypay_id FROM _contact_eligible_repeat) ";
        }
        sql+= " ORDER BY last_call " +
                " LIMIT 0,1 ";
        List<String> ids = namedJdbcTemplate.queryForList(sql, params, String.class);
        if (ids.size()<=0) throw new DataProviderException("No easypay account available!");

        String easypayId = ids.get(0);
        params.addValue("easypayId",easypayId);

        sql = " UPDATE easypay_member SET processed_on=NOW() WHERE easypay_id=:easypayId ";
        namedJdbcTemplate.update(sql, params);

        return easypayId;
    }

    public Integer getLoanSeq(Long contactId) {
        MapSqlParameterSource params = newSqlParams();
        params.addValue("contactId", contactId);

        String sql = " SELECT contract_closed FROM _contact_eligible_repeat WHERE contact_id=:contactId ";
        List<Integer> qrs = getJT().queryForList(sql, params, Integer.class);
        if (qrs.size() > 0) {
            // repeat available
            return qrs.get(0).intValue()+1;
        }

        return 1;
    }

}
