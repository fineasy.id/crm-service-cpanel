package id.fineasy.crm.cpanel.component;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import id.fineasy.crm.cpanel.utils.DbUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.User;

public abstract class ComponentAbstract {
	protected ServletContext context;	
	protected HttpServletRequest request;	
	protected NamedParameterJdbcTemplate jdbcTemplate;
	protected UserDetail userDetail;
	protected static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	protected HashMap<String,String> attributes = new HashMap<String,String>();	
	
	public ComponentAbstract() {

	}
	
	public void init(ServletContext context, HttpServletRequest request, NamedParameterJdbcTemplate jdbcTemplate, final UserDetail userDetail) {
		this.userDetail = userDetail;
		this.context = context;
		this.request = request;
		this.jdbcTemplate = jdbcTemplate;
	};	
	
	public abstract void process();
	
	public Map<String,String> getAttributes() {
		return attributes;
	}
	
	protected String generateListGridFilter(String table, String id, String name, String sortField, String condition) {		
		HashMap<String,String> sqlParams = new HashMap<String,String>();
		Map<String,Object> params = new HashMap<String,Object>();
		
		String sqlTemplate = "SELECT ${fields} FROM "+table+" WHERE "+condition;
		
		Map<String,String[]> reqMap = request.getParameterMap();
		Iterator<String> iterator = reqMap.keySet().iterator();
		while (iterator.hasNext()) {			
        	String key = (String) iterator.next();        	
        	if (key.startsWith("!")) {
        		String fieldFilter = key.replaceAll("!", "").trim();
        		sqlTemplate+= " AND `"+fieldFilter+"`=:"+fieldFilter;
        		params.put(fieldFilter, request.getParameter(key));
        	}       	
        }
		
		sqlTemplate+= " ORDER BY "+sortField;
		String sql = "";
		
		try {				
			sqlParams.put("fields", id+" as id, "+name+" as name");
			StrSubstitutor sub = new StrSubstitutor(sqlParams);
			
			sql = sub.replace(sqlTemplate);		
			
			List<Map<String, Object>> queryResult = jdbcTemplate.queryForList(sql, params);
			return DbUtils.queryToJson(queryResult).toString();
		} catch (Exception e) {
			log.error("Error: "+e,e);
		}
		return "[]";
	}
}
