package id.fineasy.crm.cpanel.component;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class QueryGenerator {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    HttpServletRequest servletRequest;

    protected static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
}
